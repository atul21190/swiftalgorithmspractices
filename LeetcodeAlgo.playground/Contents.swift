//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

func fizzBuzz(_ n:Int) -> [String] {
    var arr = [String]()
    arr.reserveCapacity(n)
    for i in 1...n {
        if i % 15 == 0 {
            arr.append("FizzBuzz")
        }
        else if i % 3 == 0 {
            arr.append("Fizz")
        }
        else if i % 5 == 0 {
            arr.append("Buzz")
        }
        else {
            arr.append(String(i))
        }
    }
    return arr
}

fizzBuzz(11)
