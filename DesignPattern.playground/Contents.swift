//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

// Factory Pattern
//Intent: In Factory pattern, we create object without exposing the creation logic to client and the client use the same common interface to create new type of object.
//The idea is to use a static member-function (static factory method) which creates & returns instances, hiding the details of class modules from user

protocol shapeInterface {
     static var s1:Int {
        get set
    }
    func shape()
}
struct circle:shapeInterface {
    static var s1: Int = 3
    
    func shape() {
        print("Circle shape")
    }
}
struct square:shapeInterface {
    static var s1: Int = 9
    
    func shape() {
        print("square shape")
    }
}
struct FactoryMethod {
    static func factory(_ type:String) -> shapeInterface? {
        if type == "Circle" {
            return circle()
        }
        else if type == "Square" {
            return square()
        }
        return nil
    }
}

// Abstract factory:

enum carModel {
    case nano,suzuki
}
enum carColor {
    case red, white
}
protocol carModelFactoryInterface {
    static func getCarOfColor(_ a:carColor)
}
struct Car {
    static var currentCarModel:carModel?

    static func nano(_ color:carColor) {
        if currentCarModel == carModel.nano {
            nanoCar.getCarOfColor(color)
        }
        else if currentCarModel == carModel.suzuki {
            suzukiCar.getCarOfColor(color)
        }
    }
}
struct nanoCar:carModelFactoryInterface {
    static func getCarOfColor(_ a:carColor)  {
        if a == carColor.red {
            print("red nano")
        }
        else if a == carColor.white {
            print("white nano")
        }
    }
}
struct suzukiCar:carModelFactoryInterface {
    static func getCarOfColor(_ a:carColor)  {
        if a == carColor.red {
            print("red suzuki")
        }
        else if a == carColor.white {
            print("white suzuki")
        }
    }
}
// final var test
class finalVarTest {
    private final var test:String
    init(_ a:String) {
        test = a
    }
}
class oveeridefinal: finalVarTest {
//    override var test:String? {          // we can not override final variable
//        get {
//        return ""
//        }
//        set {
//
//        }
//    }
}

// Agreegation, composition.

class Brain {
    var iq: Int
    init(iq: Int) {
        self.iq = iq
    }
}
class Heart {
    var bpm: Int
    init(bpm: Int) {
        self.bpm = bpm
    }
}
class Body {
    var height: Double
    var weight: Double
    init(height: Double, weight: Double) {
        self.height = height
        self.weight = weight
    }
}
class Pants {
    let color: UIColor
    let size: (Int, Int)
    init(color: UIColor, size: (Int, Int)) {
        self.color = color
        self.size = size
    }
}
class Shirt {
    let color: UIColor
    let size: String
    init(color: UIColor, size: String){
        self.color = color
        self.size = size
    }
}
class Human {

    let brain: Brain
    let heart: Heart
    let body: Body
    var pants: Pants
    var shirt: Shirt
    init(brain: Brain, heart: Heart, body: Body, pants: Pants, shirt: Shirt) {
        self.brain = brain
        self.heart = heart
        self.body = body
        self.pants = pants
        self.shirt = shirt
    }
}
let redShirt = Shirt(color: .red, size: "small")       // when human object deallocated, red shirt and blue pants still allocated as it is passed by reference and allocated in different memory location where as Heart,brain would be deallocated with Human object deallocation as it is passed as a object initialization, No one of outside keeps the refernce.
let bluePants = Pants(color: .blue, size: (32, 34))
let human = Human(brain: Brain(iq: 200),
                  heart: Heart(bpm: 60),
                  body: Body(height: 5.9, weight: 150.0),
                  pants: bluePants,
                  shirt: redShirt)

// association: -  it does not contain part whole relation like agregation, it is loose coupled dependencies, it should be done by keeping the
class Teacher {
    
}
class students {
    var list:[Teacher]? // it contain the referece which keep list of teachers.
}
// dependecies: - Dependecies can develop between object while they are associated, aggregated or composed. This kind of relationship develops while one object  invokes another objects functionality in order to accompolish some task, Any changes in callled object may break the functionality of the caller. ex: -

class GreetingSender {
    var _emailSender:EmailSender?
    func sendGreeting(_ a:EmailSender) {
        _emailSender = a
        _emailSender?.sendEmail()
    }
}
class EmailSender {
    func sendEmail() {
        
    }
}
// in this example greetings are being sent through email, The greetings sender is using the Send email() method of EmailSender object to accomplish the task, Now if any modification(introduction of parameter) is made to the SendEmail() method, then sendGreetings method of greeting sender will break, Also as per implementation, we can not send greetings y any other way, thats way my greetingsender object functionality is dependent on EmailSender objects functionality, called dependecies

// Dependecy injection: - Dependency injection is a practice about injecting functional dependency in the object. in the above example, We have injected a dependency into the Greetingssender object. We can understand the injected dependency has developed tight coupling between Greeting sender and Email sender object, We can not send greeting by any other mode only through email, So our injected dependency is violating the design principal, As a best practice we should inject dependency in such a way that objects will be decoupled and reusable.

// Inversion of control : - the term Inversion of control referes to a programming style where a framework or runtime controls the program flow. Inversion of control means we are changing the control from normal way. So for the above depency problem we can use IOC to solve DI problem.
// Solution of DI : - We should depend on abstraction of functionality not on concreate implementation of functionlity
protocol ISender {
    func send()
}
extension EmailSender:ISender {
    func send() {
        
    }
}
extension GreetingSender {
    func greetSender(_ a:ISender) {
        a.send()
    }
}
var arr = [1,2,3]
var se = Set<Int>()
se.insert(11)
var p = arr.makeIterator()

protocol component {
    var com:component {
        get set
    }
    init(_ a:component)
    func cost()
    
}
extension component {
  
}
func medianOfTwoArray(_ a:[Int], _ b:[Int]) -> Float {
    if a.count > 0 && b.count > 0 {
        var mins = 0
        let maxs = a.count
        var  i = (mins + maxs)/2
        var j = ((a.count + b.count + 1)/2) - i
        
        while(mins <= maxs) {
            var checkBool = true
            if j < b.count {
                if a[i-1] > b[j] {
                    checkBool = false
                }
            }
            if i < a.count {
                if b[j - 1] > a[i] {
                    checkBool = false
                }
            }
            if checkBool {
                let result =  max(a[i - 1],b[j - 1])
                if (a.count + b.count) % 2 == 0 {
                    if i == a.count {
                        return Float(Float(result) + Float(b[j]/2))
                    }
                    if j == b.count {
                        return Float(Float(result) + Float(a[i]/2))
                    }
                    return Float((min(a[i],b[j]) + result)/2)
                }
                return Float(result)
                
            }
            else {
                mins = i + 1
                i = (mins + maxs)/2
                j = ((a.count + b.count + 1)/2) - i
                
            }
        }
    }
    return 0
}
medianOfTwoArray([1,2,3], [4,5,6,7,8])
