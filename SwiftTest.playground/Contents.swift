//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

func UTF8Test(_ a:String) {    // UTF encoding  Test
    for i in a.utf16 {
        print(i)
    }
}
protocol protocolType {       // Protocol as a type
    //associatedtype item
    var state:Int {get set}
   // mutating func setItem(_ a:item)
}
struct protoType:protocolType {
    //typealias item = Int
    var state: Int = 3
//    mutating func setItem(_ a: Int) {
//        state = a
//    }
}
struct protoType2:protocolType {
   
    
   // typealias item = String
    var state: Int = 5
    var str:String  = "atul"
    
//  mutating func setItem(_ a: String) {
//        str = a
//    }
    
}
var arr = [protocolType]()
var type1 = protoType()
var type2 = protoType2()


var arrr = [Int]()
arrr.reserveCapacity(1)
for i in 0..<5 {
arrr.append(1)
}

protocol pen {                 // protocol extension check for constraint with protocol,(we can provide multiple defination of same method with specialized protocol)
    var color:String {get set}
    func addpen()
}
extension pen {
    var color: String {
        return "pen"
    }
    func addpen() {
      print("pen added")
    }

}

struct myPen:pen {
    var color: String = "blue"
    
    
}
struct myBook:pen {
    var color: String = "book"
    mutating func selfCheck() {
        self.color = "black"
        print(self)  // print value
    }
}
class selfTest {
    func selftestClass() {
        print(self)  // print refrence
    }
}
extension pen where Self == myBook {
    func addpen() {
        print("pen added not")
    }
}
func parameterTextElaboration(testing a:Int) {
    print(a)
}
protocol associated {
     associatedtype item
    func itemTest(_ a:item)
    func secondItemTest(_ a:item)

    
}
extension associated {
    typealias item = Int
    
    func secondItemTest(_ a:item) {
        print(a)
    }
    func itemTest(_ a:item) {
        print(a)
    }
    
}
struct associatedStruct:associated {
}


class staticTest {
    static var i = 9
    func addStatic() {
         staticTest.i = 12
        staticTest.i += 1
        print(staticTest.i)
    }
    
}


struct anyObjectTest {  // it is working, I dont know if type of property is run time, how it can be allocated in stack(but as per google, struct is wrapper on the class, internally it makes storage on heap after creating refrence on it,(means apple makes value representation and value types,array, dict..etc is value representation(allocated on heap but passed as a value type in code) or Int is value object(stack)))
    var name:Any?
   // var val:anyObjectTest? /// try to create selfloop(recursive) in struct, but it will not show compile time error, Self refrencing is allowed in class
}
//func overloadingTest() -> String {
//    return "none"
//}
//func overloadingTest(s1:String) -> String {
//    return s1
//}
//func overloadingTest(s1:String,s2:String) -> String {
//    return s1 + s2
//}
func overloadingTest(s1:String = "a",s2:String = "b",s3:String = "c") -> String {
    return s1 + s2 + s3
}
func sumAny(_ a:Any...) {
    
}
func reverseString(_ a:String) -> String {
    if a.count > 0 {
        return String(a[a.index(before: a.endIndex)]) + reverseString(String(a[a.startIndex..<a.index(before: a.endIndex)]))
    }
    return ""
}
extension String {
    static func *(_ i:String,_ a:Int) -> String {
        return  (a == 0) ?  "" :  (i + (i*(a-1)))
}
}
enum errorHandling:Error {
    case a
    case a2
    case check(fund:Int)
    
}
struct errorTest {
    var a1 = 3

    mutating func errorMethodPropagate(_ a:Int) throws {
        defer {
            a1 += 1
            print("defer2",a1)
        }
        defer {
            a1 += 2

            print("defer1",a1)
        }
        a1 += 1
        print(a1)
        if a > 2 {
            throw errorHandling.a
        }
        
        
    }
    mutating func errorMethodTryPropagate() throws {
        try errorMethodPropagate(3)
    }
    mutating func errorMethodHandleDoCatch() {
        do {
            try errorMethodTryPropagate()
        }
        catch errorHandling.a {
            print("return a")
        }
        catch {
            print("default")
        }
    }
}
protocol enumProtocol {
    func enumProtocolMethod()
}
enum enumTest:Int {     // can't create stored property and initializer, so we could not call method directly, we call method by providing raw type of enum.
    var a:Int {
        self.enumMethod()
        return 2
    }
    case atul
    case amit

    func enumMethod() {
        print("aa")
    }
    func enumProtocolMethod() {
        print("enumProtocolMethod")
    }
}
enum associatedValue {
    // it contain different types as a parameter.
    case integer(Int)
    case str(String,Int)
}
enum indirectEnumTest {         // indirect is used as recursive enum of adding associated value argument of same type as enum
    case first(String)
    case second
    indirect case third(indirectEnumTest)
}
func enumRecursionFunction(_ a:indirectEnumTest) -> Int {        // indirect enum test and switch test.
    switch a {
    case  .second:
        print("colon test")
        return 3
    case .first("aa"):
         enumRecursionFunction(.second)
         fallthrough
    case .third(.second):
        print("third")
        fallthrough
        
    default:
        print("default")
         return 2
        
    }
    return 4
}
class insideStructTest {
    
    class second {
        var a = 1
        var ele:secondStruct = secondStruct(ele:second())
    }
    struct secondStruct {
        var ele:second
    }
}
struct memberwise {
    var first:Int
    var second:Int
    var ele:UIView  // class inside struct, it will copy refernce in another variable during coping, it means refernce is same.
   
}

var stepSize = 2
func incrementInPlace(_ number: inout Int) {
    number += stepSize                       // it may produce instantneous memory conflict, currently it is not detecting by compiler but may produce unpredictable result.
}

incrementInPlace(&stepSize)

func balance(_ x: inout Int, _ y: inout Int) {
    let sum = x + y
    x = sum / 2
    y = sum - x
}
var playerOneScore = 42
var playerTwoScore = 30
balance(&playerOneScore, &playerTwoScore)  // OK
//balance(&playerOneScore, &playerOneScore)      // producing memory conflict(happening in single thread, compiler provide error)
func rethrowsTest(_ a:() throws -> Int) rethrows -> Int{
     try a()
    return 2
}
func throwsTest() throws {
    print("till now no throw")
}

var a = {() -> Int in print("rethrows test")
    return 3 }


//func neverReturnTest() -> Never {
//    myFatalError()
//}
enum myNever {
    
}

//func myFatalError() -> myNever {
//    print("aaaa")
//   // fatalError("sghsd")
//
//}

func guardFunc()  {
    var guardtest = 9
    guard guardtest > 9 else {
        fatalError()
    }
}

//polymorphism and overriding stored property
class A {         // downcast type casting not possible, parent reference can hold object of child, but child class reference can not hold object of parent.
    var firstProperty:String?
    var firstStored:String = "Red"
    {
        willSet {
            print(newValue,firstStored)
        }
        didSet {
            firstStored = "didSet"            // property obsever are not called, when we set value inside observer, it is only called when set property from class instance.
            print(oldValue,firstStored)
            
        }
    }
    var computed:String? {
        return "computed"
    }
    class var computedType:Int {  // class type is given to override computed property, otherwise static is used to stored and computed which cant be override
        return 9
        
    }
    class func typeFunction(_ a:Int) {
        print("class type function")
    }
    static var staticStored:Int? = 8
    static var computedStatic:Int {
        get {
            return 9
        }
        set {
            
        }
        
    }
    static func staticFunc() {
        print("static func")
    }
    
}
class B:A {
    override var firstProperty:String? {
        didSet {
            print("inherited stored property observer called")
        }
       
    }

    var secondProperty:String?
   // var firstStored:String = "Blue"  // not allowed because it does not make sense to overide stored property,because you can do that by changing value directly also
    
    override var firstStored:String {  // you can make stored property to computed property, this does make change for subclasses, but you can not make read only property of stored property(because )
        get {
        return "Blue"
        }
        set (value) {
            secondProperty = value
        }
     }
    //override var computed: String = "" // same we cant override to stored property, Generally speaking, We can override any property to computed property only getter setter depends of parent if property is Stored.You can also think about Liskov substitution principal whenever you are working with inheritence, what may they allow within subclasses.
    
    override var computed: String? { // allowed
        get {
            return ""
        }
        set {
            
        }
       
    }
    static var type:Int {
        get {
            return 8
        }
        set {
            
        }
    }
    override class var computedType:Int {
        get {
            return 7
        }
        set {
            
        }
    }
    override class func typeFunction(_ a:Int) {      // only class function and computed property can be overridable, static can't
        
    }
   
}

var globalVarOutsideCls = 3 {      // it is a global variable and it is always computed lazily, and it is also like stored property, can also append property observer.
    didSet {
        
    }
}
class PropertyObserverCls {
    var test:Int? = 9 {
        didSet {
            print("observer is called")
        }
        willSet {
            print("observer is about to be called")

        }
    }
    
    func observerInout(_ a:inout Int) {
        a = 5
        print("in out between observer")       // inout is copy in copy out model, thats why initially observer is not called when property is copied in temp variable, but when is wriiten back to original then it is called, but as per memory safety its write access is enable when it  gets copied in to temp variable, it means write access enabled is different from propery observer.
        

    }
    func myFunc() {
        observerInout(&test!)
    }
}

protocol dynamicDispatch {
    //func wolf()              // for using exact polymorphism, we need to declare all methods here, it uses dynamic disptach.
    func run()
}
extension dynamicDispatch {
    func wolf() {                      //when you add some methods to your struct/enum/class in extension, they will also be statically dispatched.
        print("extension wolf")
    }
}
struct dispatch:dynamicDispatch {
    func run() {
        print("struct run")
    }
    func wolf() {
        print("struct wolf")

    }
}

var a1:dynamicDispatch = dispatch()             /// polymorphism ki aisi ki taisi, using protocol oriented, because on define protocol type, variable are moved to exitenstial container which uses static dispatch table.
a1.wolf()
a1.run()

var sum = { (a:Int,b:Int) -> Int in
    a+b
}

enum myEnum {case A, B}
var enumDemo =  myEnum.A
enumDemo = .B

enum myEnum1:Int{
    case A = 1, B
}
var enumDemo1 =  myEnum1.B
print(enumDemo1.rawValue)

//let fileName = ProcessInfo.processInfo.environment["OUTPUT_PATH"]!
//FileManager.default.createFile(atPath: fileName, contents: nil, attributes: nil)
//let fileHandle = FileHandle(forWritingAtPath: fileName)!
//
//guard let number = Int((readLine()?.trimmingCharacters(in: .whitespacesAndNewlines))!)
//    else { fatalError("Bad input") }
//
//let res = validateNumber(number: number)
//
//fileHandle.write(res.data(using: .utf8)!)
//fileHandle.write("\n".data(using: .utf8)!)


func sort_names(names: [String]) -> Void {
    // Write your code here.
    if names.count > 1 {
        var test =  names.sorted{$0 < $1}
        for i in test {
            print(i)
        }
    }
    else {
        
        for i in names {
            print(i)
        }
        
    }
}

protocol requir {
    var a:Int {get set}
    init(_ ab:Int)
}
extension requir {
    init(_ ab:Int) {
        self.init(2)
        print("")
    }

}
class Abb:requir {
    var b:Int
    var a = 9
    required convenience init(_ ab: Int) {
        self.init()
    }
    init() {
        self.b = 3
    }
}
var y = [1,2]
y.drop{$0 == 1}

// protocol polymorphism
protocol polymorphism {
    func draw()
}
protocol B1:polymorphism {
    func draw()
}
extension B1 {
    func draw() {
        print("B")
    }
}
extension polymorphism {
    func draw() {
        print("polymorphism")
    }
}
struct BStruct:B1 {
   
}
struct polyStruct:polymorphism {
    
}
func polymorphismTest(_ a:polymorphism) {
    a.draw()
}
let obj1 = BStruct()
let obj2 = polyStruct()

protocol shape {
    func area() -> Any
    func permiter() -> Any
}
struct circle:shape {
    typealias T = Float
    var radius:Int
    func area() -> Any {
        return Float(3.14*Float(radius*radius))
    }
    func permiter() -> Any {
        return Float(3.14*Float(radius*2))
    }
}
struct square:shape {
    typealias T = Int
    var edge:Int
    func area() -> Any {
        return edge * edge
    }
    func permiter() -> Any {
        return 4*edge
    }
}
var myTypes = [shape]()
myTypes.append(circle(radius:2))
myTypes.append(square(edge:2))

for i in myTypes {
    print(i.area())
    print(i.permiter())
}

