//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

func anagramMapping(_ a:[Int],_ b:[Int]) -> [Int] {
    var anagramMapping:Dictionary<Int,Int>? = [:]
    var anagramArr = [Int]()
    for i in 0..<b.count {
        anagramMapping?[b[i]] = i

    }
    for i in 0..<a.count {
        anagramArr.append(anagramMapping?[a[i]] ?? 0)

    }
    return anagramArr
}

func anotherAtoI(_ str:String) -> Int {

    if str.count > 0 {
        func CheckRange(_ val:Int) -> Int {
            if -2147483648 ... 2147483647 ~= val {
                return val
            }
            else {
                if val < 0 {
                  return Int(CInt.min)
                }
                else {
                    return Int(CInt.max)
                }
            }


        }
        func overflowCheck(_ n:String) -> Int {
            if n.count > 11 {
                let obj = n.first
                if obj == "-" {
                    return Int(CInt.min)
                }
                else {
                   return Int(CInt.max)
                }
            }
            else {
               return Int(n) ?? 0
            }
        }
        var initialStringCheck = false
        var position = 1
        var myStr = ""
    for i in str.characters {
        if i == " " {
            if initialStringCheck {
                let val = overflowCheck(myStr)
                return CheckRange(val)
            }
        }
        else {
            initialStringCheck = true
            if position == 1 {
                position = 2
                switch i {
                case "+","-","0","1","2","3","4","5","6","7","8","9":
                    myStr += String(i)
                default:
                    return 0
                }
            }
            else {
                switch i {
                case "0","1","2","3","4","5","6","7","8","9":
                    myStr += String(i)
                default:
                    let val = overflowCheck(myStr)
                    return CheckRange(val)
                }
            }
        }
    }
        let val = overflowCheck(myStr)
        return CheckRange(val)

    }
    return 0
}
// MyHash
class myHash:Hashable {
    var key:Int
    
    init(_ val:Int) {
        key = val
    }
    
    public var hashValue:Int {
        return key
    }
    public static func ==(lhs: myHash, rhs: myHash) -> Bool {
        if lhs.key == rhs.key {
        return true
        }
        return false
    }
}
// Dictionary nil check for keys and Value
func myDict() {
    var dict:Dictionary<Int,Int>? = [:]
    dict = [3:4]
}
class customDict {
    var key:Any?
    subscript(index:Any) -> Any {
        get {
           return 0
        }
        set {
            
        }
    }
}

func MergeSort(_ low:Int,_ high:Int) -> [Int] {
    // temp input, it should be declared in parameter
    let input = [Int]()
    
    if low == high {
        return [input[low - 1]]
    }
    else {
        let mid = Int((low + high)/2)
        let b = MergeSort(low,mid)
        let c = MergeSort(mid + 1, high)
        return combine(b,c)
    }
}
func combine(_ a:[Int],_ b:[Int]) -> [Int] {
    var i = 0
    var j = 0
    var arr = [Int]()
    while !(i == a.count && j == b.count) {
        if i == a.count {
            for val in j..<b.count {
                arr.append(b[val])
            }
            return arr
        }
        if j == b.count {
            for val in i..<a.count {
                arr.append(a[val])
            }
            return arr
        }
        if a[i] < b[j] {
            arr.append(a[i])
            i += 1
        }
        else {
            arr.append(b[j])
            j += 1
        }
    }
    return arr
}
func findMax(_ input:[Int]) -> Int {
    
    if input.count > 0 {
        var sum = 0
        var max = Int.min
        for i in 0..<input.count {
            sum += input[i]

            if max < sum {
                max = sum
            }
            if sum < 0 {
                sum = 0
            }
        }
        return max
    }
    return 0
}
func findMaxDC(_ input:[Int]) -> Int {
    
    func getMax(_ a:Int,_ b:Int,_ c:Int) -> Int {
        var max = a;
        if b > max {
            max = b
        }
        if c > max {
            max = c
        }
        return max
    }
    func merge(_ low:Int,_ m:Int, _ high:Int) -> Int {
        var leftSum = Int.min
        var sum = 0
        for i in (low...m).reversed() {
            sum += input[i-1]
            if sum > leftSum {
                leftSum = sum
            }
        }
        var rightSum = Int.min
        sum = 0
        for i in (m+1)...high {
            sum += input[i-1]
            if sum > rightSum {
                rightSum = sum
            }
        }
        return leftSum + rightSum
    }
    func divide(_ low:Int,_ high:Int) -> Int {
        if low == high {
            return input[low-1]
        }
        else {
            let mid = Int((low + high)/2)
            let v1 = divide(low,mid)
            let v2 = divide(mid+1,high)
            return getMax(v1,v2,merge(low,mid,high))
            
        }
    }
    if input.count > 0 {
        return divide(1,input.count)
        
}
    return 0
}


// question 1. hash and hash value
//2. how overflow of integer bit moved in circular motion
//3. %d and %f coclusion, why they used in depth.
// is Array made up of dictionary

var quickResult = [Int]()
func QuickReturn(_ list:[Int]) -> [Int] {
    func Quickpartition(_ arr:[Int]) {
        var leftArr = [Int]()
        var rightArr = [Int]()
        let pivot = arr.last
        
        for i in 0..<arr.count - 1 {
            if arr[i] < pivot ?? 0 {
                leftArr.append(arr[i])
            }
            else {
                rightArr.append(arr[i])
                
            }
        }
        if leftArr.count > 0 {
            Quickpartition(leftArr)
        }
        quickResult.append(pivot!)
        if rightArr.count > 0 {
            Quickpartition(rightArr)
            
        }
    }
    if list.count > 0 {
       Quickpartition(list)
    }
    return quickResult
}



func HeapSort(_ input:inout [Int]) -> [Int] {
    for i in (0..<input.count/2).reversed() {
        Heapify(i, &input)
    }
   return deleteOps(&input)
}
func Heapify(_ index:Int,_ input:inout [Int]) {
    var currentIndex = index
    while currentIndex < input.count {
        var largestIndex = currentIndex
        if (2*currentIndex + 1) < input.count && input[(2*currentIndex + 1)] < input[largestIndex] {
            largestIndex = (2*currentIndex + 1);
        }
        if (2*currentIndex + 2) < input.count && input[(2*currentIndex + 2)] < input[largestIndex] {
            largestIndex = (2*currentIndex + 2);
        }
        if currentIndex != largestIndex {
            let temp = input[currentIndex]
            input[currentIndex] = input[largestIndex]
            input[largestIndex] = temp
            currentIndex = largestIndex
        }
        else {
            return
        }
      
    }
}
func deleteOps(_ input:inout [Int]) -> [Int] {
    var arr = [Int]()
    while input.count > 0 {
        arr.append(input.first!)
        input[0] = input.last!
        input.removeLast()
        Heapify(0, &input)
    }
    return arr
}


func ActivitySelection(_ start:[Int], _ finish:[Int]) -> [Int] {
    var arrIndex = [Int]()
    var lastFinish = Int.min
    var count = -1;
        for i in start {
            count += 1;
            if i >= lastFinish {
                arrIndex.append(count)
                lastFinish = finish[count]
            }
            
        }
    return arrIndex
}





public class TreeNode {
//    public var hashValue: Int {
//        return val
//    }
//
//    public static func ==(lhs: TreeNode, rhs: TreeNode) -> Bool {
//      return true
//    }
    
     public var val: Int
     public var left: TreeNode?
      public var right: TreeNode?
     public init(_ val: Int) {
          self.val = val
          self.left = nil
          self.right = nil
      }
  }
 

func getMinimumDifference(_ root: TreeNode?) -> Int {
    var currentMax = Int.max
    infix(root, &currentMax)

    return currentMax
}
func infix(_ root:TreeNode?,_ currentMax:inout Int) {
    if let val = root?.left {
        var min = abs((root?.val)! - val.val)
        if min < currentMax {
            currentMax = min
        }
        if let right = val.right {
        let node = findRMN(val)

        min = abs((root?.val)! - (node?.val)!)

        if min < currentMax {
            currentMax = min
        }
        }
        infix(val,&currentMax)
    }
    if let val = root?.right {
        var min = abs((root?.val)! - val.val)
        if min < currentMax {
            currentMax = min
        }
        if let left = val.left {

        let node = findLMN(val)
        min = abs((root?.val)! - (node?.val)!)
        if min < currentMax {
            currentMax = min
        }
        }
        infix(val,&currentMax)
        
    }
}
func findRMN(_ node:TreeNode?) -> TreeNode? {
    if let val = node?.right {
        return findRMN(val)
    }
        return node

  
}
func findLMN(_ node:TreeNode?) -> TreeNode? {
    if let val = node?.left {
       return findLMN(val)
    }
    return node
}
func sametree(_ root:TreeNode?,_ second:TreeNode?) -> Bool {
    
    var arr = [TreeNode?]()
    var arr2 = [TreeNode?]()

    if let val = root {
        arr.append(val)
    }
    if let val = second {
        arr2.append(val)
    }
    
    while arr.count > 0 {
        if arr.count != arr2.count {
            return false
        }
        let val1 = arr[0]
        let val2 = arr2[0]




        if (val1?.val) ?? 0 != (val2?.val) ?? 0 {
            return false
        }
        arr.removeFirst()
        arr2.removeFirst()
        if let val = val1 {
            if let value = val.left {
            arr.append(value)
            }
            else {
                arr.append(nil)

            }
            if let value = val.right {
               arr.append(value)
            }
            else {
                arr.append(nil)

            }

        }
       
        if let val = val2 {
            if let value = val.left {
                arr2.append(value)
            }
            else {
                arr2.append(nil)
                
            }
            if let value = val.right {
                arr2.append(value)
            }
            else {
                arr2.append(nil)
                
            }
        }

    }
    if arr.count != arr2.count {
        return false
    }
    return true
}
func levelOrderBottom(_ root: TreeNode?) -> [[Int]] {
    var globalArr = [[Int]]()
    var queue = [TreeNode]()
    var levelArr = [Int]()

    if let val = root {
        queue.append(val)
        levelArr.append(val.val)
        globalArr.append(levelArr)
    }
    while queue.count > 0 {
        var levelArr = [Int]()
        for i in 0..<queue.count {
        let node = queue[0]
        
        queue.removeFirst()

        if let val = node.left {
            levelArr.append(val.val)
            queue.append(val)
        }
        if let val = node.right {
            levelArr.append(val.val)
            queue.append(val)

        }
        }
        if levelArr.count > 0 {
            globalArr.append(levelArr)

        }
    }
    for i in 0..<(globalArr.count/2) {
        globalArr.swapAt(i, (globalArr.count-1) - i)
    }
    return globalArr
}

func hasPathSum(_ root: TreeNode?, _ sum: Int) -> Bool {
    guard let val = root else {
       return false
    }
    var total = 0
    func checkPath(_ node:TreeNode?,_ currentSum: Int) -> Bool {
        var currentSum = currentSum
         currentSum += node?.val ?? 0
        if let val = node?.left {
            if checkPath(val,currentSum) {
                return true
            }
        }
        else {
        if currentSum == sum,node?.left == nil,node?.right == nil {
            return true
        }
        }
        if let val = node?.right {
            if checkPath(val,currentSum) {
                return true
            }
        }
        else {
            if currentSum == sum,node?.left == nil,node?.right == nil {
                return true
            }
        }
        
        return false
    }
    if root?.left == nil,root?.right == nil,sum == root?.val  {
        return true
    }
    return checkPath(root, total)

}

func balancedTree(_ root: TreeNode?) -> Bool {
    if let val = root {
        if abs(numberOfLevel(val.left, level: 0) - numberOfLevel(val.right, level: 0)) > 1 {
            return false
        }
        else if let val = root?.left,!balancedTree(val) {
            return false
        }
        else if let val = root?.right,!balancedTree(val) {
            return false
        }
      
    }
    return true

}
func numberOfLevel(_ node:TreeNode?,level:Int) -> Int {
    var level = level
    if let _ = node {
        level += 1
       let leftLevel =  numberOfLevel(node?.left, level: level)
        
       let rightLevel = numberOfLevel(node?.right, level: level)
        if leftLevel > rightLevel {
            level = leftLevel
        }
        else {
            level = rightLevel
            
        }
    }
    return level
}
func minDepth(_ root:TreeNode?) -> Int {
    var depth = Int.max
    if let _ = root {
        calculateDepth(root, currentdepth: 0, minDepth: &depth)
    }
    else {
        depth = 0
    }
    return depth
}
func calculateDepth(_ node:TreeNode?,currentdepth:Int,minDepth:inout Int) {
    if let _ = node {
        var currentdepth = currentdepth
        currentdepth += 1
        if  currentdepth >= minDepth {
            return
        }
        if node?.left != nil || node?.right != nil {
            calculateDepth(node?.left, currentdepth: currentdepth, minDepth: &minDepth)
            calculateDepth(node?.right, currentdepth: currentdepth, minDepth: &minDepth)

        }
        else {
            minDepth = currentdepth
        }
    }
}
func checkSymmetric(_ root:TreeNode?) -> Bool {
    var nodeArr = [TreeNode?]()
    var front = -1
    var rear = -2
    var nodeCount = 0
    if let val = root {
        nodeArr.append(val)
        front = 0
        rear = 1
        if root?.left != nil || root?.right != nil {
            
        }
        else {
            return true
        }
        
    }
    while front < rear {
        var arrInt = [Int?]()
        nodeCount = (rear - front)
        while nodeCount > 0 {
            let node = nodeArr[front]
            front += 1
            if node != nil {
                nodeArr.append(node?.left)
                nodeArr.append(node?.right)
                rear += 2
                
            }
            arrInt.append(node?.val)
            nodeCount -= 1
            
        }
        if !checkPolindrom(arr: arrInt) {
            return false
        }
       add()
        
    }
    return true
}
func add() -> String {
    return "abc"
}
 func checkPolindrom(arr:[Int?]) -> Bool {
    for i in 0..<arr.count {
        if arr[i] != arr[(arr.count-1) - i] {
            return false
        }
    }
    return true
}
func validPalindrom(_ str:String?) -> Bool {
    if let val = str,val.count > 1 {
        var i = str?.startIndex
        var j = str?.endIndex
        if i != j {
         j = str?.index(before: j!)
        }
    let alpha = CharacterSet.alphanumerics

    while i! < j! {
        let char1 = str?[i!]

        let char2 = str?[j!]


        var firstPointer:Character?
        var secondPointer:Character?

        if alpha.contains((char1?.unicodeScalars.first)!) {
            firstPointer = char1
        }
        else {
            i = str?.index(after: i!)
        }
        if alpha.contains((char2?.unicodeScalars.first!)!) {
            secondPointer = char2
        }
        else {
            j = str?.index(before: j!)
        }
        if let _ = firstPointer, let _ = secondPointer{
            if String(describing: firstPointer).caseInsensitiveCompare(String(describing: secondPointer)) != ComparisonResult.orderedSame {
                return false
            }
            else {
                i = str?.index(after: i!)
                j = str?.index(before: j!)
                
            }
        }
    }
        
    }
    return true
}
class numbers {
    var next:numbers?
    var val:Int
    init(_ value:Int) {
       val = value
    }
}
func moveZeroes(_ nums: inout [Int]) -> [Int] {
    if nums.count > 1 {
        var head:numbers? = numbers(0)
    var p = head
    var countZero = 0
    for i in nums {
        if i != 0 {
            p?.next = numbers(i)
            p = p?.next
          
        }
        else {
            countZero += 1
        }
    }
    nums.removeAll()
        head = head?.next
    while head != nil {
        nums.append((head?.val)!)
        head = head?.next
    }
    while countZero > 0 {
        nums.append(0)
        countZero -= 1
    }
    }
    return nums
    
}
func moveZero(_ nums: inout [Int]) {
    if nums.count > 1 {
    var zeroIndex = -1
    var nonZero = -1
    for i in 0..<nums.count {
        let val = nums[i]
        if val != 0 {
            nonZero = i
        }
        else {
            if zeroIndex == -1 {
                zeroIndex = i
            }
           
        }
        if nonZero != -1,zeroIndex != -1,zeroIndex < nonZero {
            nums.swapAt(zeroIndex, nonZero)
            zeroIndex += 1
            nonZero = -1
        }
        
    }
    }
}
class MyList:Hashable {
    var hashValue: Int {
        return val.hashValue
    }
    
    static func ==(lhs: MyList, rhs: MyList) -> Bool {
        return true
    }
    
    var next:MyList?
    var val:Int
    init(_ value:Int) {
        val = value
    }
    
}
func cycleInList(_ list:MyList?) -> Bool {
    if list != nil {
        var list = list
        var set = Set<MyList>()
        set.insert(list ?? MyList(Int.min) )

        while list?.next != nil {
            if set.contains((list?.next)!) {
                return true
            }
            else {
                list = list?.next
                set.insert(list!)
            }
        }
        
    }
    return false
}
func binarySearch(_ arr:[Int],target:Int) -> Int {
    var arr = arr
    
    func mySearch(_ low:inout Int,_ high:inout Int) -> Int {
        if low == high {
            if arr[low] == target {
                return low
            }
            else if arr[low] < target {
                return low + 1
            }
            else {
                return low
            }
        }
        else if low < high {
            let mid = (high + low)/2
            if arr[mid] == target {
                return mid
            }
            else if arr[mid] > target {
                high = mid - 1
                return mySearch(&low, &high)
            }
            else {
                low = mid + 1

               return  mySearch(&low, &high)
            }
        }
        return low
    }
    if arr.count > 0 {
        var low = 0
        var high = (arr.count - 1)
        return mySearch(&low, &high)
    }
    return 0
}


//func isBst(_ node:TreeNode?) -> Bool {
//    if node != nil {
//        var a = [TreeNode?]()
//
//        var dict:Dictionary<TreeNode,TreeNode> = [:]
//        dict[node!] = nil
//
//        var parent:TreeNode?
//        a.append(node)
//        var front = 0
//        var rear = 0
//        while front <= rear {
//            let p = a[front]
//            parent = dict[p!]
//            if p != nil {
//                if p?.left != nil {
//                    if !((p?.left?.val)! < (p?.val)! && ((p?.left?.val)! < (parent?.val) ?? Int.max )) {
//                        return false
//                    }
//                    else {
//                        a.append(p?.left)
//                        dict[(p?.left)!] = p
//                        rear += 1
//
//                    }
//                }
//                if p?.right != nil {
//                    if !((p?.right?.val)! > (p?.val)! && ((p?.right?.val)! > (parent?.val) ?? Int.min )) {
//                        return false
//                    }
//                    else {
//                        a.append(p?.right)
//                        dict[(p?.right)!] = p
//                        rear += 1
//
//                    }
//                }
//
//                front += 1
//            }
//        }
//    }
//    return true
//
//}
// Dp problem

func maxRobber(_ list:[Int]) -> Int {
   var a = 0
    var b = 0
    
    for i in 0..<list.count {
        if i%2 == 0 {
            a = max(a + list[i], b)
        }
        else {
            b = max(b + list[i], a)

        }
    }
    
    return max(a, b)
}
func max(_ a:Int,_ b:Int) -> Int {
    if a > b {
        return a
    }
    return b
}
func nonDecreasingSeq(_ nums:[Int]) -> Int {
    if nums.count > 0 {
        var currentMax = 1
        var dict:Dictionary = [Int:Int?]()
        for i in 0..<nums.count {
            dict[nums[i]] = 1
            for j in 0..<i {
                if nums[j] < nums[i] {
                    dict[nums[i]] = max((dict[nums[j]]!! + 1),(dict[nums[i]]!!))
                }
            }
            currentMax = max((dict[nums[i]] ?? 0)!, currentMax)
        }
        return currentMax
    }
    return 0
}
func minClimb(_ nums:[Int]) -> Int {
    //if nums.count > 0 {
        var dict:Dictionary<Int,Int?> = Dictionary.init(minimumCapacity: nums.count)
      
        for i in 0..<nums.count {
            let val1 = nums[i] + (dict[i - 2] ?? 0)!
            let val2 = nums[i] + (dict[i - 1] ?? 0)!
            dict[i] = max(val1,val2)
        }
        let val1 = (dict[nums.count - 1] ?? 0)!
        let val2 = (dict[nums.count - 2] ?? 0)!
        return max(val1,val2)
   // }
  //  return 0
}
func maxProfit(_ nums:[Int]) -> Int {
    if nums.count > 0 {
        var buy = nums[0]
        var profit = 0
        for i in 1..<nums.count {
            if buy > nums[i] {
                buy = nums[i]
            }
            else {
                profit = max((nums[i] - buy), profit)
            }
        }
        return profit
    }
    return 0
}
class trie:Hashable {
    var hashValue: Int {
        return 0
    }
    static func ==(lhs: trie, rhs: trie) -> Bool {
        return true
    }
    var val:Character?
    var parent:trie?
    init(_ value:Character?,_ pp:trie?) {
        self.val = value
        self.parent = pp
    }
    
    var children = Dictionary<Character,trie?>()
}
func address(o: UnsafePointer<Void>) -> Int {
    return unsafeBitCast(o, to: Int.self)
}
//func longestCommonPrefix(_ strs:[String]) -> Int {
//
//    var curreantMax = 0
//    var current = Dictionary<Character,trie?>()
//    func getDict(_ dict: inout Dictionary<Character,trie?>)  {
//        current = dict
//       // print(address(o: &current))
//    }
////    func recursionTrie(_ dict:[Character:trie?],_ val:Int) {
////        var value = val
////         value += 1
////
////        for i in dict.keys {
////            print(i)
////            recursionTrie((dict[i]??.dict)!,value)
////        }
////    }
//    if strs.count > 0 {
//        var test = trie.init(nil, nil)
//       // var obj = test.dict
//
//        for i in strs {
//            var value = 0
//           // getDict(&test.dict!)
//            for chars in i.characters {
//                if current[chars] == nil {
//                    current[chars] = trie()
//                    current[chars]??.dict = Dictionary<Character,trie?>()
//
//                    print(address(o: &(current[chars] as! trie).dict!))
//
//                    getDict(&(current[chars] as! trie).dict!)
//                    print(chars)
//                }
//                else {
//                    var dict = current[chars]??.dict
//                    getDict(&dict!)
//                    value += 1
//                    print(chars)
//                    curreantMax =  max(value, curreantMax)
//                }
//            }
//        }
//       // var value = -1
//      // recursionTrie(current, value)
//
//    }
//
//    return curreantMax
//}
//extension Array:Hashable {
//    public var hashValue: Int {
//        return 1
//    }
//
//    public static func ==(lhs: Array<Element>, rhs: Array<Element>) -> Bool {
//        return true
//    }
//
//
//}
func CourseBooktest(_ numCourses:Int,_ list:[[Int]]) -> Bool {
    var topological = [Int]()
    var queue = [Int]()

    var inDegree = [Int]()
    var vistedVertex = [Bool]()
    for i in 0..<numCourses {
        inDegree.append(0)
        vistedVertex.append(false)
    }
    var adjSet = [[Bool]]()
    adjSet.reserveCapacity(numCourses*numCourses)
    
    for i in 0..<numCourses {
        var arr = [Bool]()
        for j in 0..<numCourses {
            arr.append(false)

        }
        adjSet.append(arr)
    }

    for i in list {
        inDegree[i[1]] += 1
        adjSet[i[0]][i[1]] = true
    }

    for i in 0..<inDegree.count {
        if inDegree[i] == 0 {
            queue.append(i)
            vistedVertex[i] = true
            
        }
    }
    
    while queue.count > 0 {
        var vertex = queue[0]
        topological.append(vertex)
        queue.removeFirst()
        for i in 0..<numCourses {
            if adjSet[vertex][i], !vistedVertex[i] {
                inDegree[i] -= 1
            if inDegree[i] == 0 {
                queue.append(i)
                vistedVertex[i] = true

            }
            }
        }
        
    }
    if topological.count == numCourses {
    return true
    }
    return false

}
func cycleTest(_ number:Int,_ list:[[Int]]) -> Bool {
    if number > 0 {
    var visitedSet = [(Bool,Int,Int)]()
    var startTime = 0
    var adjList = [[Bool]]()
    for i in 0..<number {
        var tuple = (false,0,0)
        visitedSet.append(tuple)
        var arr = [Bool]()
        for j in 0..<number {
            arr.append(false)
        }
        adjList.append(arr)
    }
    for i in list {
        adjList[i[0]][i[1]] = true
    }
   
    func dfsTraversal(node:Int) -> Bool {
        startTime += 1
        var endTime = 0

        visitedSet[node] = (true,startTime,endTime)
        for i in 0..<number {
            var tup = visitedSet[i]
                if adjList[node][i] {
                    if !tup.0 {

                    if !dfsTraversal(node: i) {
                        return false
                    }
                }
                    else {
                        if !(tup.2 > 0) {
                            return false
                        }
                    }
            }
            
            
        }
        endTime = startTime + 1
        visitedSet[node] = (visitedSet[node].0,visitedSet[node].1,endTime)
        startTime = endTime
        return true
    }
        for i in 0..<number {
            if !visitedSet[i].0 {
            if !dfsTraversal(node: i) {
                return false
            }
            }
        }
    }
    return true

}

@objc
protocol myProtocol {
    
    var tt:Int {
        get
    }
    
    //    func nameFunc() {
    //      print("hh")
    //    }
}

 class testAbstraction:myProtocol {
    
    var tt: Int {
        get {
            return 7
        }
        
        set {
            
        }
    }
     static var name2: Int {
        get {
            return 3
        }
        set {
            
        }
    }

    var num = 0
    var num1:Int?
    
    func nameTest() {
        tt = 8
        print(tt)
    }
}
class bb:testAbstraction {
    var nose:Int?
}
extension testAbstraction {
    var ftd:Int {
        return 7
    }
}

protocol protocolOriented {
    var kk:Int {
        get
    }
    
}
//extension protocolOriented {
//    var kk:Int {
//        return 12
//    }
//}
//struct jj:protocolOriented {
//
//}
extension Int:protocolOriented {
    var kk: Int {
        return 5
    }
    
    
}
extension String:protocolOriented {
    var kk: Int {
        return 3
    }
    
    
}


func firstUniqChar(_ s: String) -> Int {
    if s.count == 0 {
        return -1
    }
    if s.count == 1 {
        return 0
    }
    var dict = [Character:Int]()
    for c in s.characters {
        if let val = dict[c] {
            dict[c] = val + 1
        }
        else {
            dict[c] = 1
        }
        
    }
    var count = -1
    for (n,c) in s.enumerated() {
        count += 1
        if dict[c] == 1 {
            return count
        }
    }
    return -1
}
class nodeArr {
    var val = 0
    var link:[nodeArr?] = [nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
}
func trieTest(_ str:[String]) -> String {
    var currentNode = nodeArr()
    var head = currentNode
    var currentVal = Int.max
    var currentStr:String = ""
    var count = 0
    for i in str {
        var longest = 0
        count += 1
        currentNode = head
        var prefixStr:String = ""
        if i == "" {
            return ""
        }

        for j in (i.lowercased()).unicodeScalars {
            if currentNode.link[(Int(j.value) - 97)] != nil {
                longest += 1
                prefixStr = prefixStr + String(j)
                currentNode = currentNode.link[(Int(j.value) - 97)]!
            }
            else {
                currentNode.link[(Int(j.value) - 97)] = nodeArr()
                currentNode = currentNode.link[(Int(j.value) - 97)]!
            }
        }
        if count > 1, longest == 0 {
            return ""
        }
        if longest <= currentVal,longest != 0 {
            currentVal = longest
            currentStr = prefixStr
           // return currentStr
        }
        else if longest == 0,currentStr.count > 0 {
            return ""
        }

        else if str.count > 1,currentVal == 0 {
             currentStr = ""
            
        }
        else if str.count == 1 {
            currentStr = i

        }
        
        
    }
    return currentStr
}
func findKthLargest(_ k:Int,_ arr:[Int]) -> Int {
    var arr = arr
    buildMaxHeap(&arr)
    var largest:Int?
    for i in 1...k {
       largest =  deletefromHeap(&arr)
       heapify(&arr, 0)
    }
    return largest ?? 0
}
func buildMaxHeap(_ arr: inout [Int]) {
    for i in (0..<arr.count/2).reversed() {
        heapify(&arr, i)
    }
}
func deletefromHeap(_ arr:inout [Int]) -> Int? {
    arr.swapAt(0, arr.count - 1)
    var temp = arr.last
    arr.removeLast()
    return temp
    
}
func heapify(_ arr:inout [Int],_ i:Int) {
    var left = 2*i + 1;
    var right = 2*i + 2;
    if arr.count > right {
     if arr[left] > arr[right],arr[left] > arr[i] {
        arr.swapAt(i, left)
        heapify(&arr, left)
    }
     else if arr[right] > arr[i] {
        arr.swapAt(i, right)
        heapify(&arr, right)

    }
    }
    else if arr.count > left,arr[left] > arr[i] {
        arr.swapAt(i, left)
        heapify(&arr, left)
        
    }
}
var dictMinPath = [Int:Int]()
func findMinPath(_ arr:[[Int]]) -> Int {
     dictMinPath = [Int:Int]()

    var arr = arr
    if arr.count > 0,arr[0].count > 0 {
    return findPath(&arr, currentDownIndex: 0, currentRightIndex: 0)
    }
    return 0
}
func findPath(_ arr: inout [[Int]],currentDownIndex:Int,currentRightIndex:Int) -> Int {
    let m = arr.count
    let n = arr[0].count
    let temp = arr[currentDownIndex][currentRightIndex]
    var Downvalue:Int?
    var Rightvalue:Int?

    if currentDownIndex + 1 < m {
        if let val = dictMinPath[(currentDownIndex + 1)*n + currentRightIndex] {
            Downvalue = temp + val
        }
        else {
       Downvalue =  temp + findPath(&arr, currentDownIndex:currentDownIndex + 1 , currentRightIndex: currentRightIndex)
        }
    }
    if currentRightIndex + 1 < n {
        if let val = dictMinPath[(currentDownIndex)*n + currentRightIndex + 1] {
            Rightvalue = temp + val
        }
        else {
       Rightvalue =  temp + findPath(&arr, currentDownIndex:currentDownIndex , currentRightIndex: currentRightIndex + 1)
        }
        
    }
    if let _ = Downvalue,let _ = Rightvalue {
        if Downvalue! <= Rightvalue! {
            dictMinPath[currentDownIndex*n + currentRightIndex] = Downvalue!

            return Downvalue!
        }
        dictMinPath[currentDownIndex*n + currentRightIndex] = Rightvalue!

        return Rightvalue!

    }
    else if let _ = Downvalue {
        dictMinPath[currentDownIndex*n + currentRightIndex] = Downvalue!

        return Downvalue!
    }
    else if let _ = Rightvalue {
        dictMinPath[currentDownIndex*n + currentRightIndex] = Rightvalue!

        return Rightvalue!
    }
    else {
        dictMinPath[currentDownIndex*n + currentRightIndex] = temp
        return temp
    }
}

func powerOfTwo(_ a:Int8) -> Bool {
    if a == 0 {
        return false
    }
    if (a & (a - 1)) == 0 {
        return true
    }
    return false
}
func powerOfFour(_ a:Int8) -> Bool {
   // var b = a
    if a == 0 || a < 0 {
        return false
    }
    if (a & (a - 1)) == 0 {
       // var count = 0
     //   while b > 0 {
            if (a & 0x55) != 0 {
                return true
               // count += 1
            }
           // b = b >> 1
     //   }
        //if count%2 == 0 {
        //    return true
       // }
    }
    return false
}
func missingNumber(_ a:[Int]) -> Int {

    let b = a.count
    let sum = Int((Float(b + 1)/2) * Float(b))
    var arrSum = 0
    for i in a {
        arrSum += i
    }
    return sum - arrSum
}
func majorityNumber(_ a:[Int]) -> Int {
    
    var dict = [Int:Int]()
    for i in a {
        print(i)
        if let _ = dict[i] {
            dict[i]! += 1
        }
        else {
            dict[i] = 1
        }
    }
    for i in dict.keys {
        if dict[i]! > (a.count/2) {
            return i
        }
    }
    return 0
}
func singleNumber(_ a:[Int]) -> Int {
    var result = 0
    for i in a {
        result = result ^ i
    }
    return result
}
func twoSum(_ a:Int,_ b:Int) -> Int {
    if b == 0 {
        return a
    }
    else {
        return twoSum(a ^ b, (a&b) << 1)
    }
}
func findTheDifference(_ s: String, _ t: String) -> Character {
    var resultS = UInt32(0)
    var resultT = UInt32(0)

    for c in 0..<s.unicodeScalars.count {
        var s1 = s[c]
        resultS += (s1.unicodeScalars.first?.value)!
        
    }
    for c in 0..<t.unicodeScalars.count {
        var s1 = t[c]
        resultT += (s1.unicodeScalars.first?.value)!
        
    }
   
    return Character(UnicodeScalar(resultT - resultS)!)
}
extension String {
    subscript (i: Int) -> Character {
        get {
        return self[index(startIndex, offsetBy: i)]
        }
    }
}
func Subset(_ a:[Int]) -> [[Int]] {
    var intDec = 1 << a.count
    var arrSub = [[Int]]()
    for j in 0..<intDec {
        arrSub.append([Int]())
    }

    for i in 0..<a.count {
        var arr = [Int]()
        for j in 0..<intDec {
            if (j >> i) & 1 == 1 {
               arrSub[j].append(a[i])
            }
        }
    }
    return arrSub
}
func reverse(_ n:UInt8) -> UInt8 {
    var a:UInt8 = n
    a = ((a & 0xf0) >> 4) | ((a & 0x0f) << 4);
    a = ((a & 0xcc) >> 2) | ((a & 0x33) << 2);
    a = ((a & 0xaa) >> 1) | ((a & 0x55) << 1);
    return a;
}
func reverseString(_ s:String) -> String {
    var arr = Array(s.characters)
    arr.reverse()
//    for i in 0..<s.count/2 {
////        var a1 = s[(s.count - 2) - i]
////        var a2 = s[i]
////         var temp = a1
////         a1 = a2
////         a2 = temp
//        arr.swapAt(i, (s.count - 1) - i)
//
//    }
    return String(arr)
}
func coinChange(_ a:[Int],_ target:Int) -> Int {
    var global = [Int]()
    func recursion(_ index:Int,_ sum:Int,_ count: Int) -> Bool {
        let count1 = count + 1
        let sum1 = sum + a[index]
        if sum1 == target {
            global.append(count1)
            return true
        }
        else if (index + 1) >  a.count {
            return recursion(index + 1, sum1, count1)
        }
        else {
            return false
        }
    }
    for i in 0..<a.count {
       // var sum = 0
       //var count = 0
        recursion(i,0,0)
    }
    return minSelection(global)
   
}
func minSelection(_ a:[Int]) -> Int {
    if a.count > 0 {
        var t = a[0]
        for i in 1..<a.count {
            if a[i] < t {
                t = a[i]
            }
        }
        return t

    }
    return -1
}
func reverseVowels(_ s: String) -> String {
    var set = Set<Character>()
    set = ["a","e","i","o","u","A","E","I","O","U"]
    var charArr = Array(s)
    var i = 0
    var j = charArr.count - 1
    var startVowel:Character?
    var endVowel:Character?
    
    while (j - i) > 0 {
        if startVowel == nil, set.contains(charArr[i]){
            startVowel = charArr[i]
        }
        else if !set.contains(charArr[i]) {
            i += 1
        }
        if endVowel == nil, set.contains(charArr[j]){
            endVowel = charArr[j]
        }
        else if !set.contains(charArr[j]) {
            j -= 1
        }
        if let _ = startVowel,let _ = endVowel {
            charArr.swapAt(i, j)
            i += 1
            j -= 1
            startVowel = nil
            endVowel = nil
        }
    }
    return String(charArr)
}
func myPow(_ x: Double, _ n: Int) -> Double {
    var dict = [Int:Double]()
    func pow1(_ x1:Double, _ n1:Int) -> Double {

        let x1 = x1
        let n1 = n1
        if n1 == 1 || n1 == -1 {
            if n > 0 {
            return x1
            }
            else {
                return 1/x1
            }
        }
        else {

            let mid = abs(n1)/2
            var midval = 0.0
            var remainingval = 0.0
            var remaining = 0
            if let val = dict[mid] {
               midval = val
            }
            else {
             midval = pow1(x1, mid)
             dict[mid] = midval
            }

             remaining = abs(n1) - (mid)

            if let val = dict[remaining] {
                remainingval = val
            }
            else {
             remainingval = pow1(x1, remaining)

            dict[remaining] = remainingval
            }
            let value = midval * remainingval

            dict[abs(n1)] = value
            return value

            
        }
    }
    if n == 0 {
        return 1
    }
   

    return pow1(x, n)

   
   
}
protocol atul {
    var name1:String {
        get set
    }
    func setName()
    //init(nm:String)
}
struct abc {
    var name:String
//    init(nm:String) {
//        name = nm
//    }
}
extension atul {
    func setName() {
        print("aa")
    }

}
extension atul where Self:UIView {
    func setName() {
        print("da")
    }
    
}

class cfd:UIView,atul {
    var name1: String
    
//    func setName() {
//
//    }
    

   // var name1: String
//      init(nm: String) {
//        name1 = ""
//
//       // super.init(nm: name1)
//
//    }
    init() {

        name1 = ""
       // super.init(nm: name1)
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension Int {

    static postfix func ++ (_ a:Int) -> Int {
        return a + 1
    }
}
infix operator +-+-
func +-+- (_ a:Int,_ b:Int) -> Int {
    return a + b
}

func maxProduct(_ a:[Int]) -> Int {
    if a.count > 0 {
   // var cm = 1
        var min:Int = 1
        var index = 0
    var dp = [Int:Int]()
    func maxValue(i:Int) {
        if a[i] == 0 {
            dp[i] =  1
          //  cm = 1
            min = 1
            return
        }
       
        if a[i] > 0 {
            if (dp[i-1] ?? 0) > 0 {
                dp[i] = a[i]*dp[i-1]!

            }
            else {
                dp[i] = a[i]
                
            }
           // cm = dp[i]!
        }
        else {
            if min < 0 {
                if i - 1 == index && index != 0 {
                    dp[i] = min*a[i]*(dp[index] ?? 1)
                }
                else if i - 1 == index && index == 0 {
                    dp[i] = a[i]*(dp[index] ?? 1)

                }
                else {
                    dp[i] = min*a[i]*(dp[index] ?? 1)

                }
               
                min = 1
               // cm = dp[i]!
            }
            else {
                min = a[i]
                index = i
                if i == 0 {
                    dp[i] = min

                }
                else {
                dp[i] = dp[i - 1] ?? 0
                }

               // cm = 1
            }
        }

        
        
    }
        for j in 0..<a.count {
            maxValue(i: j)

        }
        var val = dp[0]!

        for i in 1..<a.count {

            if val < dp[i]! {
                val = dp[i]!
            }
        }
        return val
    }
    return 0
}



func maxIssue(_ a:[Int]) -> Int{
    if a.count > 0 {
    var r = a[0]
    var max1 = r
    var min1 = r
    for i in 1..<a.count {
    if a[i] < 0 {
        var temp = max1
        max1 = min1
        min1 = temp
        }
        max1 = max(max1*a[i], a[i])
        min1 = min(min1*a[i], a[i])
        r = max(max1, r)
        
    }
    return r
    }
    return 0
}
class liitleIndian {
    var a:UInt16 = 1
    var f = "a"
    var g:UInt16 = 0x22

     func val() {
        var data = NSMutableData()
        var number : UInt32 = 0x1234567
        var convertedNumber = number.littleEndian
        data.append(&convertedNumber, length: 4)
        print(g.bigEndian)
        
    }
}
func compilationDirective() {
var sum = 1 & 1
var sumf = 4
#if sum
    sumf = 10
#else
    sumf = 12
#endif
}
func MaxProfitBuySell(_ a:[Int]) -> Int {
    var temp = -1
    var buy = -1
    var sell = -1
    var profit = 0
    for i in a {
        if temp == -1 {
            temp = i
        }
        if buy != -1 && i > temp {
            temp = i
        }
        if buy == -1 && i < temp {
            temp = i
        }
        if buy == -1 && i > temp {
            sell = -1
            buy = temp
            temp = i
        }
        if sell == -1 && i < temp && buy != -1 {
            sell = temp
            profit = profit + (sell  - buy)
            temp = i
            buy = -1
        }
    }
    if buy != -1 && a.last! > buy {
        profit = profit + (a.last!  - buy)

    }

 
    return profit
}
func fillContainer(_ container:Int,_ bottle:[Int]) -> Int {
    if bottle.count > 0 {
    var bottle = bottle.sorted()
    var max = 0
    var index = -1
    var dict = [Int:Int]()
    var count = 0
    for j in 0..<bottle.reversed().count where max < container {
        print(bottle)
        var i = bottle[j]
        index += 1
        max += i
        
        if max > container {
           // max -= i
            break
        }
        else {
            dict[i] = i
            count += 1
            if max == container {
                return count
            }
        }
    }
    if abs(container - max) != 0 {
        max -= bottle[index]
    for i in index..<bottle.count {
        var val = bottle[i]
        print(val)
        var remain = container - max
        var valRemain = val - remain
        if let val = dict[valRemain] {
            return count
        }
    }
    }
        return count

    }
    return 0
}
struct queue {
    var arr = [Int]()
    var front = -1
    var rear = -1

    mutating func enqueue(_ ele:Int) {
        arr.append(ele)
        rear += 1
        
    }
    mutating func dequeue(_ ele:Int) {
        if !arr.isEmpty {
        arr.remove(at: front)
        front -= 1
        }
    }
}

struct stackUsingQueue {
    var que = queue()
    func push(_ ele:Int) {
        
    }
    func pop(_ ele:Int) {
        
    }
}
class superDeepCopy:NSObject {
    var t = 8
    override func copy() -> Any {
        let obj = superDeepCopy()
        obj.t = self.t
        return obj
    }
}
class deepCopy:NSObject {
    var a = 5
    var obj = superDeepCopy()
//    func copy(with zone: NSZone? = nil) -> Any {
//        return self
//    }
    func test() {
        a = 9
        obj.t = 89
    }
    override func copy() -> Any {
        let ob = deepCopy()
        ob.obj = self.obj.copy() as! superDeepCopy
        return ob
    }
    
}


class myCode:Codable {
    var name = "atul"
}
enum key:CodingKey {
    case first
}
extension myCode{
    func encoding() -> Data? {
        let json = JSONEncoder()
        return try? json.encode(self)
    }
    func decoding(_ a:Data) -> myCode? {
        let json = JSONDecoder()
        return try? json.decode(myCode.self, from:a)
    }
}
struct encodeMyClass {
    var name = "aa"
}

extension encodeMyClass:Decodable {
      init(from decoder: Decoder) throws {
        let val = try?decoder.container(keyedBy: key.self)
        let name = try? val?.decode(String.self, forKey: .first)
    }
}
extension encodeMyClass:Encodable {
    func encode(to encoder: Encoder) throws {
        var val = encoder.container(keyedBy: key.self)
        try?val.encode(name, forKey: .first)
    }
}
var currentMax = 0

func longestPaththathavesameNodeInTree(_ node:TreeNode?) -> Int {
    var leftCount = 0
    var rightCount = 0
    if let val = node?.left {
        leftCount =  longestPaththathavesameNodeInTree(val)
        if node?.val != val.val {
            currentMax = max(currentMax, leftCount)
            leftCount = 0
        }
        else {
            leftCount += 1
            
        }
        
    }
   
    if let val = node?.right {
        rightCount =  longestPaththathavesameNodeInTree(val)

        if node?.val != val.val {
            currentMax = max(currentMax, rightCount)

            rightCount = 0
        }
        else {
            rightCount += 1
        }
    }
   
    if  node?.left?.val == node?.val, node?.right?.val == node?.val {
        leftCount += rightCount


    }
    if leftCount > rightCount {
        currentMax = max(currentMax, leftCount)

        return leftCount
    }
    else {
        currentMax = max(currentMax, rightCount)

        return rightCount
    }
}
func prepareNode(_ a:[Any]) {
    
    
}
var arr = [1,2,nil]
//prepareNode(arr)
var node = TreeNode(2)
node.left = TreeNode(2)
node.right = TreeNode(1)
node.right?.right = TreeNode(1)


[26,26,26,26,26,24,26,25,25,25,27,23,25,25,27,24,26,24,26,24,24,null,28,null,null,26,null,null,26,26,28,25,null,25,27,null,null,null,null,null,23,null,null,29,27,null,null,null,null,25,null,27,27,24,26,24,26,26,26,null,22,28,null,26,26,null,null,26,null,28,28,25,null,null,null,25,25,25,27,25,25,27,25,null,null,null,null,null,null,null,27,27,27,null,null,27,29,24,26,26,26,null,26,null,26,null,null,null,24,24,24,null,26,24,26,null,null,null,26,null,null,null,28,null,30,null,23,27,null,null,null,null,null,null,null,null,null,null,null,23,25,25,25,27,25,23,25,null,null,null,null,null,null,29,null,null,null,26,null,22,null,null,26,24,26,null,26,28,null,null,26,22,null,null,null,null,null,null,null,null,null,null,25,23,null,null,null,null,27]
