//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

func mergeForKruskal(_ graph:([[Int]],[Int])) -> ([[Int]],[Int]) {
    func merge( _ low:Int,_ high:Int) -> ([[Int]],[Int]) {
        if low == high {
            var tuple = ([graph.0[low]],[graph.1[low]])
            return tuple
        }
        else if low < high {
            var mid = (low + high)/2
           return mergeSortedList(merge(low, mid), merge(mid + 1, high))
        }
        var tuple = ([graph.0[low]],[graph.1[low]])
        return tuple
        
    }
    func mergeSortedList(_ a:([[Int]],[Int]), _ b:([[Int]],[Int])) -> ([[Int]],[Int])  {
        var a = a
        var b = b
        var i = 0
        var j = 0
        var tuple = ([[Int]](),[Int]())
        while j < b.1.count && i < a.1.count {
            if a.1[i] > b.1[j] {
                tuple.0.append(b.0[j])
                tuple.1.append(b.1[j])
                j += 1
            }
            else {
                tuple.0.append(a.0[i])
                tuple.1.append(a.1[i])
                i += 1
            }
    }
        while i < a.1.count {
            tuple.0.append(a.0[i])
            tuple.1.append(a.1[i])
            i += 1
        }
        while j < b.1.count {
            tuple.0.append(b.0[j])
            tuple.1.append(b.1[j])
            j += 1
        }
        return tuple
        
    }
    if graph.1.count > 0 {
       return merge(0, graph.1.count - 1)
    }
    return graph
}
func KruskalAlgo(_ graph:([[Int]],[Int])) -> Int {        // not working
    var dictAdj = [Int:[Int]]()
    func detectCycle(_ a:[Int]) -> Bool {
        var visited = Set<Int>()
        visited.insert(a[0])
        var parent = Int.min
        var current = a[0]
        func dfs(_ node:Int) -> Bool {
            if let val = dictAdj[node] {
                for i in val {
                    if visited.contains(i) && i != parent {
                        print("done")
                        removeAdj(a)
                        return false
                    }
                    else if visited.contains(i) {
                        
                    }
                    else {
                        parent = node
                        visited.insert(i)
                        if !dfs(i) {
                        return false
                        }
                    }
                }
                
            }
            return true
            
        }
        return dfs(current)
    }
    func createAdjList(_ adj:[Int]) {
        let first = adj[0]
        let second = adj[1]
        if var val = dictAdj[first] {
            val.append(second)
            dictAdj[first] = val
            
        }
        else {
            dictAdj[first] = [second]
        }
        if var val = dictAdj[second] {
            val.append(first)
            dictAdj[second] = val
            
        }
        else {
            dictAdj[second] = [first]
        }
    }
    func add(_ a:[Int]) -> Bool {
        createAdjList(a)
        return detectCycle(a)
    }
    func removeAdj(_ a:[Int]) {
        print("remove",a)
        print(dictAdj)

        if var val = dictAdj[a[0]] {
            val =  val.filter{$0 != a[1]}
            dictAdj[a[0]] = val
        }
        else {
            dictAdj[a[0]] = nil
        }
        if var val = dictAdj[a[1]] {
            val =  val.filter{$0 != a[0]}
            dictAdj[a[1]] = val
        }
        else {
            dictAdj[a[1]] = nil
        }
        print(dictAdj)
    }

    var soretedTuple = mergeForKruskal(graph)
    var count = -1
    var total = 0
    for i in soretedTuple.1 {
        count += 1
        if add(soretedTuple.0[count]) {
            total += i
        }
    }
   // detectCycle([3,1])
    return total
}


func minimumCoin(_ a:[Int],_ amount:Int) -> Int {             /////not working
    if amount <= 0 {
        return 0
    }
    if a.isEmpty {
        return 0
    }
   var a =  a.sorted()
    var global = allSubset(a)
    func calculate(_ arr:[Int],_ pos:Int, _ amo:Int) -> Int {
        if pos == 0 {
            return -1
        }
        if amo % arr[pos - 1] == 0 {
            return amo/arr[pos - 1]
        }
        else {
            let val = calculate(arr,pos - 1,amo % arr[pos - 1])
            if val != -1 {
                return (amo/arr[pos - 1]) + val
            }
            else {
                return -1
            }
        }
    }
    for i in global.reversed() {
        if i.last! <= amount {
            let val =  calculate(i,i.count,amount)
            if val != -1  {
                return val
            }
        }
    }
    return -1
    
}
func allSubset(_ a:[Int]) -> [[Int]] {
    var globalSet = [[Int]]()
    globalSet.append([Int]())
    for i in a {
        for j in globalSet {
            var j = j
            j.append(i)
            globalSet.append(j)
        }
    }
    globalSet.removeFirst()
    return globalSet
}

func primesAlgo(_ a:[[Int]],_ node:Int) {
    var dictAdj = [Int:[[Int]]]()
    for i in a {
        dictAdj = createAdjList(i, dictAdj)
    }
    print(dictAdj)
    var vistedMst = Set<Int>()
    vistedMst.insert(a[0][0])
    var adjMst = [[Int]]()
    adjMst.append([0,0])

    for i in 1...node {
        adjMst.append([i,Int.max])
    }

    var minWeight = 0
    while vistedMst.count <= node {
        print(adjMst)
        var traversed = extractMin(&adjMst,vistedMst)
        minWeight += traversed[1]
        vistedMst.insert(traversed[0])
        for i in dictAdj[traversed[0]]! {
            var ele = adjMst[i[0]]
            ele[1] = i[1]
            adjMst[i[0]] = ele
        }
    }
    print(minWeight)
}
func extractMin( _ a: inout [[Int]],_ vis:Set<Int>) -> [Int] {
    var min = [Int]()
    
  

    for i in a {
        if min.isEmpty && !vis.contains(i[0]) {
            min = i
        }
        else if min.count > 0 && i[1] < min[1] && !vis.contains(i[0]) {
            min = i
        }
        
    }
    return min
}

func buildHeap(_ a:[[Int]]) {
    var a = a
    for i in 0..<a.count/2 {
        heapify(&a, i)
    }
}
func heapify(_ a: inout [[Int]],_ i:Int) {
    var left = 2*i + 1
    var right = 2*i + 2
    
    if left < a.count {
        a[left][1] < a[i][1]
        a.swapAt(left, i)
        heapify(&a, left)
    }
    if right < a.count {
        a[right][1] < a[i][1]
        a.swapAt(right, i)
        heapify(&a, right)
    }
}
func deleteMin(_ a:inout [[Int]]) -> [Int] {
    a.swapAt(0, a.count - 1)
    return a.last!
}
func createAdjList(_ adj:[Int],_ dictAdj:[Int:[[Int]]]) -> [Int:[[Int]]] {
    var dictAdj = dictAdj
    let first = adj[0]
    let second = adj[1]
    let wt = adj[2]
    if var val = dictAdj[first] {
        var arr = [Int]()
        arr.append(second)
        arr.append(wt)
        val.append(arr)
        dictAdj[first] = val
    }
    else {
        var arr = [Int]()
        arr.append(second)
        arr.append(wt)
        dictAdj[first] = [arr]
    }
    if var val = dictAdj[second] {
        var arr = [Int]()
        arr.append(first)
        arr.append(wt)
        val.append(arr)

        dictAdj[second] = val
    }
    else {
        var arr = [Int]()
        arr.append(first)
        arr.append(wt)
        dictAdj[second] = [arr]
    }
    return dictAdj
}

var weightedEdgeList = [[0,1,2],[0,2,4],[1,3,5],[3,4,3],[2,4,3]]
primesAlgo(weightedEdgeList, 4)

struct errtr:Error {
    
}
