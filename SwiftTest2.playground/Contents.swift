//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let point = (12,13)        // Swift bind the value with variable and accepet that case, also if genaral binding does not require default case as it know any once case definately match.

switch point {
 case (4,13):
     print("ok")
     print("done")
case (14,13):
    print("16")
case (var x,let y):
    x = 3
   // print(x,y)
}

// joined higher function use, It joined the content of array and return raw value instead of array.

func joinedTest(_ names:[Int]...) {
    var test = Array(names.joined())
    var filter = test.filter({$0 > 1})
    var map = names.map({arr in
        arr.map({$0 * 10})
    })
    print(test)
    print(filter)
    print(names)
}
joinedTest([1],[2])

func myMap(_ a:(([Int]) -> [Int])) -> [Int] {
    var a = a([1,2])
    return a
}
myMap({ var mappedArr = [Int]()
    for ele in $0 {
      mappedArr.append(ele * 10)
    }
    return mappedArr
})

// Array initialization:

let arr = Array(1..<10)
var t = [1,2,3,4,5].map({$0 * 10})
print(t)
let names = ["Pilot": "Wash", "Doctor": "Simon"]
let doctor = names["doctor"] ?? "Atul"

let crew = NSMutableDictionary()
crew.setValue("Kryten", forKey: "Mechanoid")
var crewCopy = crew
crewCopy = ["crewKey": "crewValue"]
crew

var max = Int.max
max &+ 1

//var typeInference:Int = 12.0 // Implicit type conversion not allowed

//let optionalOutOfBound = [String?]()
//
//if let name = optionalOutOfBound[10] {     // will produce error
//    print("Brought to you by \(name)")
//}

import Foundation
let ns = NSString(string: "asd")
let swift = String(String(ns))

struct Spaceship {
    var name: String {
        willSet {
            print("I'm called \(newValue)!")  // willValue produce newValue in parameter
        }
        didSet (value) {
            print("I'm called \(value)")    // didSet produce oldValue in parameter.
            
        }
    }
}

let oneMillion = 01_000_000_____   // swift allow this kind of representation(adding underscore between digits) to read numbers
let oneThousand = oneMillion / 0_1_0_0_0

//for i in 3...1 {    // range bound is checked on runtime
//    print(i)
//}
//var i = 2

//do {                 // do while is not allowed in swift, do is replaced with repeat, and do keyword is used in do, try for error handling

//    print(i)
//    i *= 2
//} while (i < 128)

struct structInheritence {
    
}
//struct inheritence2:structInheritence {  // inheritence in value type not supported, only conform protocol
//
//}

class singlton {
    static let singleObj = singlton()       // in objective c, constant does not work for immutability of object,it make constant of refrence address which does not make sense  while in swift, compiler dont allow to change let variable,that why objective c singlton needed dispath_once to make thread safe.
    private init() {
        
    }
  
}

class user:UserDefaults {
    
    override class var standard: UserDefaults {
        return UserDefaults()
    }
}
func tupleInoutTest(_ a: inout Int, _ b:inout Int) {
    print(a)
}
func incrementInPlace(_ number: inout Int) {
    number += number
}
//@objc dynamic func observer() {       // for adding observer of notification at runtime
//
//}
infix operator <^>

extension Int{
    static func <^> (_ a: inout Int, _ b: inout Int) -> Int {
        return a * b
    }
}
var obj = user.standard
var obj1 = user.standard
Unmanaged.passUnretained(obj).toOpaque()
Unmanaged.passUnretained(obj1).toOpaque()

struct Decoratar {
    func cost() -> Int {
        return 5
    }
}
extension Decoratar {
    func mangoFoodDecorator(_ item:Decoratar) -> Int {
        return 10 + item.cost()
    }
}
extension Decoratar {
    func bananaFoodDecorator(_ item:Decoratar) -> Int {
        return 20 + item.cost()
    }
}

// escaping and non escaping closure.
var escapingArr = [() -> Void]()

func escapingclosure(_ a: @escaping () -> Void) {
    escapingArr.append(a)
    a()
}
var i = 0
var esp = {
    i += 1
print("appended",i)
}
escapingclosure(esp)
for i in escapingArr {
    i()
}
func autoClosure(_ a: @autoclosure () -> String) {
    a()
}
//autoClosure(numberList.remove(at: 0))

var total = 0
func captureClosure(_ a:Int) -> () -> Void {
    total += a
    let inter = { total += 1
        print(total)}
    inter()
    return inter
}

class test {
    var a = 9
    var str:strongReferenceClosure?
}
class strongReferenceClosure {
    var a = 2
    unowned(unsafe) var f:test
    init(_ a1:test) {
        f = a1
    }
    var cls:(() -> Void)?
   
    func testStrongReference() -> () -> Void {
        cls = {
            [unowned self] in
            self.a = 3
        }
        cls!()
        return cls!
    }
}
//var strs:strongReferenceClosure? = strongReferenceClosure()
//var test = strs?.testStrongReference()
//strs = nil
//print(test!())

//var t1:test? = test()
//var d1:strongReferenceClosure = strongReferenceClosure(t1!)
//t1?.str = d1
//t1 = nil
//print(d1.f)




