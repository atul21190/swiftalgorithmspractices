//: Playground - noun: a place where people can play

import UIKit
import Foundation

// access control.

//var str = "Hello, playground"
//
//var stepSize = 3
//
//func incrementInPlace(_ number: inout Int) {
//    number += stepSize
//}
//
//incrementInPlace(&stepSize)
//
//struct Player {
//    var name: String
//    var health: Int
//    var energy: Int
//
//    static let maxHealth = 10
//    mutating func restoreHealth() {
//        health = Player.maxHealth
//    }
//}
//
//func balance(_ x: inout Int, _ y: inout Int) {
//    let sum = x + y
//    x = sum / 2
//    y = sum - x
//}
//extension Player {
//    mutating func shareHealth(with teammate: inout Player) {
//        balance(&teammate.health, &health)
//    }
//}
//
//var oscar = Player(name: "Oscar", health: 10, energy: 10)
//var maria = Player(name: "Maria", health: 5, energy: 10)
//oscar.shareHealth(with: &maria)  // OK
////oscar.shareHealth(with: &oscar)
//
//indirect enum ArithmeticExpression {
//    case number(Int)
//    case addition(ArithmeticExpression, ArithmeticExpression)
//    case multiplication(ArithmeticExpression, ArithmeticExpression)
//}
//let five = ArithmeticExpression.number(5)
//let four = ArithmeticExpression.number(4)
//let sum = ArithmeticExpression.addition(five, four)
//let product = ArithmeticExpression.multiplication(sum, ArithmeticExpression.number(2))
//
//
//func evaluate(_ expression: ArithmeticExpression) -> Int {
//    switch expression {
//    case let .number(value):
//        return value
//    case let .addition(left, right):
//        return evaluate(left) + evaluate(right)
//    case let .multiplication(left, right):
//        return evaluate(left) * evaluate(right)
//    }
//}
//
//(evaluate(product))
//enum SomeEnumeration {
//    // enumeration definition goes here
//}
//enum CompassPoint:String {
//    case north
//    case south
//    case east
//    case west
//}
//
//var directionToHead = CompassPoint.west
//directionToHead = .east
//directionToHead = .south
//switch directionToHead {
//case .north:
//    print("Lots of planets have a north")
//case .south:
//    print("Watch out for penguins")
//case .east:
//    print("Where the sun rises")
//case .west:
//    print("Where the skies are blue")
//}
//
//
//enum Barcode {
//    case upc(Int, Int, Int, Int)
//    case qrCode(String)
//}
//var productBarcode = Barcode.upc(8, 85909, 51226, 3)
////productBarcode = .qrCode("ABCDEFGHIJKLMNOP")
//switch productBarcode {
//case .upc(let numberSystem, let manufacturer, let product, let check):
//    print("UPC: \(numberSystem), \(manufacturer), \(product), \(check).")
//case .qrCode(let productCode):
//    print("QR code: \(productCode).")
//}
//enum ASCIIControlCharacter: Character {
//    case tab = "\t"
//    case lineFeed = "\n"
//    case carriageReturn = "\r"
//}
//enum Planet: Int {
//    case mercury = 1, venus, earth, mars, jupiter, saturn, uranus, neptune
//}
//let earthsOrder = Planet.earth.rawValue
//let sunsetDirection = CompassPoint.west.rawValue
//let possiblePlanet = Planet(rawValue: 7)
//let positionToFind = 11
//if let somePlanet = Planet(rawValue: positionToFind) {
//    switch somePlanet {
//    case .earth:
//        print("Mostly harmless")
//    default:
//        print("Not a safe place for humans")
//    }
//} else {
//    print("There isn't a planet at position \(positionToFind)")
//}
//class abc {
//    var a:String
//    var b:Float
//    init(c:String,f:Float) {
//        a = c
//       // let th = self.a;
//        //print(th);
//        b = f
//    }
//    func did() {
//
//    }
//}
//class bcd:abc {
//   // var t:String
//
//    init(h:String,y:String,g:Float) {
//       // t = h
//
//        super.init(c: h, f: g)
//        done()
//
//
//
//
//
//    }
//    override init(c: String, f: Float) {
//       // t = c
//        super.init(c: c, f: f)
//
//    }
//    convenience init(hi:String,y:String,g:Float) {
//
//        self.init(h: hi, y: y, g: g)
//
//    }
//    func done() {
////        defer {
////            print("gf")
////            print("hfhg")
////        }
////        defer {
////            print("gf")
////            print("shfhg")
////        }
//        print("s")
//
//    }
//
//}
//enum err:Error {
//    case invalid
//}
//class test {
//    func loadImage(atPath:String) throws -> err {
//        defer {
//            print("gf")
//            print("shfhg")
//        }
//        throw err.invalid;
//
//
//    }
//}
//var d:bcd?
//var a:abc?
//
//
////a = bcd(h:"ss",y:"s",g:3.0)
////d = abc(c: "q", f:3)
//
//
//
//var tc = test()
//do {
//let abcd = try tc.loadImage(atPath: "./Resources/John Appleseed.jpg")
//}
//catch {
//}
////print(abcd)
//
//struct Item {
//    var price: Int
//    var count: Int
//
//}
//class MediaItem {
//    var name: String
//    init(name: String) {
//        self.name = name
//    }
//}
//class Movie: MediaItem {
//    var director: String
//    init(name: String, director: String) {
//        self.director = director
//        super.init(name: name)
//    }
//}
//
//class Song: MediaItem {
//    var artist: String
//    init(name: String, artist: String) {
//        self.artist = artist
//        super.init(name: name)
//    }
//}
//let library = [
//    Movie(name: "Casablanca", director: "Michael Curtiz"),
//    Song(name: "Blue Suede Shoes", artist: "Elvis Presley"),
//    Movie(name: "Citizen Kane", director: "Orson Welles"),
//    Song(name: "The One And Only", artist: "Chesney Hawkes"),
//    Song(name: "Never Gonna Give You Up", artist: "Rick Astley")
//]
//for item in library {
//    if item is Movie {
//        print("Movie: \((item as MediaItem).name), dir. \((item as! Movie).director)")
//    } else if let song = item as? Song {
//        print("Song: \(song.name), by \(song.artist)")
//    }
//}
//var things = [Any]()
//
//things.append(0)
//things.append(0.0)
//things.append(42)
//things.append(3.14159)
//things.append("hello")
//things.append((3.0, 5.0))
//things.append(Movie(name: "Ghostbusters", director: "Ivan Reitman"))
//things.append({ (name: String) -> String in "Hello, \(name)" })
//
//for thing in things {
//    switch thing {
//    case 0 as Int:
//        print("zero as an Int")
//    case 0 as Int:
//        print("zero as a Double")
//    case let someInt as Int:
//        print("an integer value of \(someInt)")
//    case let someDouble as Double where someDouble > 0:
//        print("a positive double value of \(someDouble)")
//    case is Double:
//        print("some other double value that I don't want to print")
//    case let someString as String:
//        print("a string value of \"\(someString)\"")
//    case let (x, y) as (Double, Double):
//        print("an (x, y) point at \(x), \(y)")
//    case let movie as Movie:
//        print("a movie called \(movie.name), dir. \(movie.director)")
//    case let stringConverter as (String) -> String:
//        print(stringConverter("Michael"))
//    default:
//        print("something else")
//    }
//}
//let names = ["Chris", "Alex", "Ewa", "Barry", "Daniella"]
//
//
//func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
//    for i in 0..<nums.count {
//        for j in 1..<nums.count {
//            if nums[i] + nums[j] == target {
//                return [i,j]
//            }
//        }
//    }
//    return [0,0]
//}func removeDuplicates(_ nums: inout [Int]) -> Int {
//public class ListNode {
//        public var val: Int
//        public var next: ListNode?
//          public init(_ val: Int) {
//                self.val = val
//                     self.next = nil
//             }
//     }
//func mergeTwoLists(_ l1: ListNode?, _ l2: ListNode?) -> ListNode? {
//    var l3:ListNode? = ListNode(0);
//    var c1 = l1;
//    var c2 = l2;
//    var c3 = l3;
//    while(c1 != nil || c2 != nil) {
//        if c1 != nil && c2 == nil {
//           l3?.val = (c1?.val)!
//           c1 = c1?.next
//            l3?.next = ListNode(0)
//            l3 = l3?.next
//
//
//        }
//       else if c2 != nil && c1 == nil {
//            l3?.val = (c2?.val)!
//            c2 = c2?.next
//            l3?.next = ListNode(0)
//            l3 = l3?.next
//
//        }
//       else if((c1?.val)!>(c2?.val)!) {
//            l3?.val = (c2?.val)!
//            c2 = c2?.next
//            l3?.next = ListNode(0)
//            l3 = l3?.next
//        }
//        else {
//            l3?.val = (c1?.val)!
//            c1 = c1?.next
//            l3?.next = ListNode(0)
//            l3 = l3?.next
//        }
//    }
//    l3 = nil
//    return c3
//}
//
//var l1 = ListNode(4)
//l1.next = ListNode(2)
//
//var l2 = ListNode(1)
//l2.next = ListNode(5)
// var c = mergeTwoLists(l1, l2)
//
//func twoSum(_ nums: [Int], _ target: Int) -> Int {
//    var low = 0;
//    var high = 180
//    var mid = Int(floor(Double(high + low)/2))
//
//    while low != mid {
//        if mid*mid == target {
//            return mid
//        }
//        else if mid*mid > target {
//            high = mid - 1;
//            mid = Int(floor(Double(high + low)/2))
//        }
//        else if  mid*mid < target {
//            low = mid + 1;
//            mid = Int(floor(Double(high + low)/2))
//        }
//
//    }
//    if low*low < target {
//        return low
//    }
//    if low*low > target {
//        return low - 1
//    }
//    return low;
//
//}
//
//
//var arr = [0,1,2,4]
//let r = twoSum(arr,4567);
//
//public class TreeNode {
//    public var val: Int
//    public var left: TreeNode?
//    public var right: TreeNode?
//    public init(_ val: Int) {
//        self.val = val
//        self.left = nil
//        self.right = nil
//    }
//}
//
//class Solution {
//    var globalArr = [[Int]]()
//    func levelOrder(_ root: TreeNode?) -> [[Int]] {
//        if let val = root {
//            var arr1 = [TreeNode?]()
//            arr1.append(val)
//            return myFunc(arr:arr1)
//
//
//        }
//        return []
//    }
//    func myFunc(arr:[TreeNode?]) -> [[Int]] {
//        var arr2 = [Int]()
//        var customArr = [TreeNode?]()
//        for i in 0..<arr.count {
//            var val  = arr[i]
//            arr2.append(val?.val ?? 0)
//
//
//            if let left = val?.left {
//                customArr.append(left)
//            }
//            if let right = val?.right {
//                customArr.append(right)
//            }
//
//        }
//        globalArr.append(arr2)
//
//        if customArr.count > 0 {
//            myFunc(arr:customArr)
//
//        }
//        return globalArr;
//    }
//}
//var v = Solution()
//var cu = TreeNode.init(3)
//cu.left = TreeNode.init(5)
//cu.right = TreeNode.init(10)
//v.levelOrder(cu)

var sdf = DispatchQueue(label: "asd", qos: DispatchQoS.background, attributes: DispatchQueue.Attributes.concurrent, autoreleaseFrequency:DispatchQueue.AutoreleaseFrequency.never, target: DispatchQueue.global())

//var fg = DispatchQueue(label: "assd", qos: DispatchQoS.background, attributes: DispatchQueue.Attributes.concurrent, autoreleaseFrequency:DispatchQueue.AutoreleaseFrequency.never, target: DispatchQueue.global())
//fg.asyncAfter(deadline: DispatchTime.init(uptimeNanoseconds: 1000), execute:{
//    print("amit12");
//})
//fg.async {
//
//    print("gg")
//
//
//}
//fg.async {
//    sleep(1)
//
//    print("asd1")
//}
//
//fg.sync {
//    print("asd2")
//}
//
//fg.sync {
//    print("asd3")
//}
//
//fg.sync {
//    print("asd4")
//}



//sdf.sync {
//    print("atul");
//}

//sdf.asyncAfter(deadline: DispatchTime.init(uptimeNanoseconds: 1000), execute:{
//    print("amit1");
//})
//sdf.async {
//    print("amit2");
//}
//DispatchQueue.main.async {
//    print("done");
//
//}
//var q1 = DispatchQueue(label: "a");
//q1.sync {
//    for i in 0..<1
//    {
//        print("q1111",i)
//    }
//
//}
//q1.async {
//    for i in 0..<12 {
//        print("q2",i)
//    }
//}
//var arrP = [false,false]
//var turn = -1;var str = "atul"
//
//
//sdf.async {
//    arrP[0] = true
//    turn = 1;
//
//    while arrP[1] == true && turn == 1  {
//
//    }
//    str = "amit0"
//    arrP[0] = false
//}
//sdf.async {
//    arrP[1] = true
//    turn = 0;
//
//    while arrP[0] == true && turn == 0  {
//
//    }
//    str = "amit1"
//    arrP[1] = false
//}
//print(str)
//sdf.sync {
//    print("amit3");
//}
//sdf.sync {
//    print("amit4");
//}
//
//
//sdf.sync(execute: {
//    print("aaa")
//})

var dict  = Dictionary<Int,Set<Int>>()
var dictD  = Dictionary<Set<Int>,Int>()
var localSet = Set<Set<Int>>()

var dictMediater  = Dictionary<Int,Int>()

var pathCounter = 0;
var globalSet = Set<Int>()
var leavesSet = Set<Int>()
var deletedSet = Set<Int>()


var minimum = 0;
var pathList = [Int]()


func findMinHeightTrees(_ n: Int, _ edgeList: [[Int]]) -> [Int] {
    print("all good")
    if edgeList.count == 0 || n == 0 {
        return [0]
    }
    for edge in edgeList {
        if edge.count != 0 {
        let edgeFirstElement = edge.first
        let edgeSecondElement = edge.last
   
        if let setFirst = dict[edgeFirstElement!] {
            var setFirst = setFirst
            setFirst.insert(edgeSecondElement!)
            dict[edgeFirstElement!] = setFirst

        }
        else {
            var setFirst = Set<Int>()
            setFirst.insert(edgeSecondElement!)
            dict[edgeFirstElement!] = setFirst


        }
        if let setSecond = dict[edgeSecondElement!] {
            var setSecond = setSecond

            setSecond.insert(edgeFirstElement!)
            dict[edgeSecondElement!] = setSecond


        }
        else {
            var setSecond = Set<Int>()
            setSecond.insert(edgeFirstElement!)
            dict[edgeSecondElement!] = setSecond


        }
    }
        else {
            return [0]
        }
    }
    for i in 0..<n {
        globalSet.insert(i)
    }
    while true {
        if dict.keys.count > 2 {
            for i in dict.keys {
                print("current",i)
                let adjacent = dict[i]
                //adjacent = adjacent?.subtracting(deletedSet)
                if (adjacent?.count)! > 1 {
                    
                }
                else {
                   dict.removeValue(forKey: i)
                    print(dict)
                    globalSet.remove(i)
                    deletedSet.insert(i)
                }
            }
            for i in dict.keys {
                var adjacent = dict[i]
                adjacent = adjacent?.subtracting(deletedSet)
                dict[i] = adjacent

            }
        }
        else {
//            var arr = [Int]()
//            arr = Array(globalSet)
        return Array(globalSet)
        }
    }
//    for i in 0..<n {
//        for j in i+1..<n {
//            globalSet.removeAll()
//
//            sourceDestination(source: i, destination: j)
//        }
 //       print("atul....",i)
//        if let val = dictMediater[i] {
//          pathList.append(val)
//        }
//        else {

        
        
            
      //  }
  
for i in globalSet {
print("current",i)
    leavesSet.insert(i)
    print(leavesSet)

    if let substractinList = (dict[i]?.subtracting(leavesSet)) {
    print(substractinList)
    if (substractinList.count) > 0 {

        deleteLeaves(_adjacency:substractinList)
    }
    else {
        //globalSet.remove(i)
        }
 }

}
    
   // return globalSet

   // print("pathlist",pathList)
   // return findMinElementList(list: pathList, minElement: findMinElement(list: pathList))

}
func deleteLeaves(_adjacency:Set<Int>) {
    var adjacency = _adjacency;

    for element in _adjacency {
print("ele",element)
        if let _adjacencySet = dict[element] {
            let substractinList = (_adjacencySet.subtracting(leavesSet))
        if (substractinList.count) > 0 {
            leavesSet.insert(element)
            deleteLeaves(_adjacency: substractinList)
         }
        else {
            globalSet.remove(element)
            dict[element] = nil
            deletedSet.insert(element)
            print("deleted",element)
            print("global",globalSet)


         }
        }
 }
}
func sourceDestination(source:Int,destination:Int) -> Int {
    pathCounter = 0
    globalSet.insert(source)
    
    
    
    
//    if let _adjacencySet = dict[i] {
//
//        pathList.append(checkPath(_adjacency: _adjacencySet,key:i,items:n,pathcount:1,locaCount: 0));
//        //minimum =   findMinElement(list: pathList)
//
//    }
  return 0
}
func checkPath(_adjacency:Set<Int>,key:Int,items:Int,pathcount:Int,locaCount:Int) -> Int {
 //  var pathCount += 1
    var _adjacency = _adjacency
  //  globalSet = globalSet.union(_adjacency)
//    var _localset = Set<Int>()
//    _localset.insert(key)
//    _localset.insert(element)
//    dictD[_localset] = pathcount

    if checkAllElemenet(_adjacency: globalSet,key:key,items:items) {

        return pathcount
        
    }
    else {
        globalSet = globalSet.union(_adjacency)

        var arrayPath = Array<Int>()
    for element in _adjacency {
       // var localPath = 0;
       
        var _localset = Set<Int>()
        _localset.insert(key)
        _localset.insert(element)
        if let val = dictD[_localset]  {
//            print(_localset)
//
//            print(val)
            arrayPath.append(val);

           // return val
        }
        else {
           // print("pathhhh",pathcount)
        dictD[_localset] = pathcount
        }
            arrayPath.append(pathcount);

        let _adjacencySet = dict[element];
        let substractinList = (_adjacencySet?.subtracting(globalSet))
        if (substractinList?.count)! > 0 {
//            if pathcount+1 > minimum && minimum != 0 {
//                return 1000
//            }
          let path =  checkPath(_adjacency:substractinList! , key: key, items: items,pathcount: pathcount + 1,locaCount: locaCount + 1)
            if path > 0 {
                //localPath += 1

                arrayPath.append(path);
             
            
          }
        }
     }
       // arrayPath.append(pathcount);
//print("array ",arrayPath)
        let maxPath = findMaxElement(list: arrayPath)
       // print("maxpath",maxPath,key)
        if maxPath == 0 {
            return pathcount;
        }
       // dictMediater[key] = locaCount + 1

//        if pathcount > locaCount + 1 {
//            dictMediater[key] = pathcount
//        }
//        else {
//            dictMediater[key] = locaCount + 1
//
//        }
      //  print(dictMediater)
        pathCounter = maxPath
    }
                //print(dictD)

    return pathCounter
}
func checkAllElemenet(_adjacency:Set<Int>,key:Int,items:Int) -> Bool {
    var count = 0
    for i in 0..<items {
            if _adjacency.contains(i) {
                count += 1;
            }
        
    }

    if count == items {
        return true
    }
    return false
    
}
func findMinElement(list:Array<Int>) -> Int {
    if list.count > 0 {
        
    var smallest = list.first
    for i in list {
        if i < smallest! {
            smallest = i
        }
    }
        
        return smallest!

    }
    return 0
}
func findMaxElement(list:Array<Int>) -> Int {
    if list.count > 0 {
        
        var largest = list.first
        for i in list {
            if i > largest! {
                largest = i
            }
        }
        return largest!
        
    }
    return 0
}
 func findMinElementList(list:Array<Int>,minElement:Int) -> [Int] {
    if list.count > 0 {
    var minPathList = [Int]();
    var count = -1;
    for i in list {
        count += 1
        if i == minElement {
            minPathList.append(count)
        }
    }
    return minPathList
    }
    return [0]
}

//var edgeList =  [[0,1],[1,2],[1,3],[2,4],[4,6],[3,5]]
//let edgeList:Any =
//var edgeList =  [[0,1],[1,2],[2,3]]
//var _zip = zip(edgeList)
//print(edgeList)
//var val = findMinHeightTrees(10, edgeList)
//print("valu",val)



  public class TreeNode {
      public var val: Int
     public var left: TreeNode?
      public var right: TreeNode?
    public var parent:TreeNode?
     public init(_ val: Int) {
          self.val = val
          self.left = nil
          self.right = nil
      }
  }
 class Solution {
    var arr = [Int]()
    //var parentNode:TreeNode?
    var isLeftVisted = false
    func inorderTraversal(_ root: TreeNode?) -> [Int] {
        if root != nil {
            var leftRoot = root
           // parentNode = root

        while true {
           // print(leftRoot?.parent?.val)
            if leftRoot?.left == nil && leftRoot?.right == nil {
               arr.append((leftRoot?.val)!)
                leftRoot = nil
                leftRoot = leftRoot?.parent
                if leftRoot == nil {
                    return arr
                }
            }
           else if leftRoot?.left != nil {
                //isLeftVisted = true
                //parentNode = leftRoot
                leftRoot = leftRoot?.left
                

            }
            else if leftRoot?.right != nil {
                //parentNode = leftRoot
                leftRoot = leftRoot?.right
                //isLeftVisted = false


            }
            }
        }
        return arr
    }
    func leftTree(_ left:TreeNode?) {
        if let val = left {
            inorderTraversal(val)
        }
    }
    func rightTree(_ right:TreeNode?) {
        if let val = right {
            inorderTraversal(val)
        }
    }
}
var list = TreeNode(0)
list.parent = nil
//list.left = TreeNode(1)
list.right = TreeNode(2)
list.right?.parent = list

list.right?.left = TreeNode(3)
list.right?.left?.parent = list.right
let ss = Solution()
var _inorder = ss.inorderTraversal(list)


 public class ListNode {
      public var val: Int
      public var next: ListNode?
      public init(_ val: Int) {
          self.val = val
          self.next = nil
      }
  }
 

//func removeNthFromEnd(_ head: ListNode?, _ n: Int) -> ListNode? {
//
//    var count = 0
//    var startNode = head
//    var previous = head
//    var baseHead = head
//
//    while baseHead != nil {
//        count += 1
//        baseHead = (baseHead?.next) ?? nil
//    }
//    count = count - n
//    for i in 0..<count {
//        startNode = (startNode?.next) ?? nil
//        if i == count - 1 {
//            previous?.next = (startNode?.next) ?? nil
//        }
//        else {
//            previous = (previous?.next) ?? nil
//        }
//
//    }
//    return head
//}
func removeNthFromEnd(_ head: ListNode?, _ n: Int) -> ListNode? {
    var count = 0
    var previous = head
    var baseHead = head
    if head == nil || n == 0 {
        return head
    }
    while let _ = baseHead?.next {
        baseHead = baseHead?.next ?? nil
        count += 1
        if count > n {
            previous = previous?.next ?? nil
        }
    }
    if (n - count) == 1 {
        previous = previous?.next ?? nil
        return previous
    }
    else {
        previous?.next = previous?.next?.next ?? nil
    }
    return head
}
// BFS Algo.
//var arr = [TreeNode]()
//var invertedTree:TreeNode?
//var invertedArr = [TreeNode]()
//
//
//func BFSAlgo(_ head: TreeNode?) {
//    if head != nil {
//        arr.append(head!)
//        vistedList()
//
//    }
//    else {
//        return
//    }
//
//}
//func vistedList() {
//    while arr.count > 0 {
//        let obj = arr.first
//        print(obj?.val ?? 0)
//
//        if let val = obj?.left {
//            arr.append(val) // enqueue
//
//        }
//        if let val = obj?.right {
//            arr.append(val)
//
//        }
//        arr.removeFirst()  // dequeue
//        vistedList()
//    }
//}
func BFSInvertedAlgo(_ head: TreeNode?) -> TreeNode? {
    var arr = [TreeNode]()
    if head != nil {
        arr.append(head!)
       return vistedInvertedList(arr,invertedList: head)
        
    }
    else {
        return nil
    }
    
}
func vistedInvertedList(_ bfsList:[TreeNode],invertedList:TreeNode?) -> TreeNode? {
    var arr = bfsList
    while arr.count > 0 {
        let obj = arr.first
        
        let leftVal = obj?.left
        obj?.left = obj?.right
        if let val = obj?.left {
            arr.append(val) // enqueue
            
        }
        obj?.right = leftVal
        
        if let val = obj?.right {
            arr.append(val) // enqueue
            
        }

        arr.removeFirst()  // dequeue

    }
    return invertedList
}
var bfsList = TreeNode(1)
bfsList.left = TreeNode(2)
bfsList.right = TreeNode(7)
bfsList.left?.left = TreeNode(8)
bfsList.right?.right = TreeNode(3)
bfsList.right?.right?.right = TreeNode(1)

var listTree:TreeNode? = BFSInvertedAlgo(bfsList)
print(listTree?.left?.left?.left?.val)

//var arrLoop = [Int]()
//func testingLoopcount() {
//
//    while arrLoop.count < 10 {
//        arrLoop.append(2)
//        print("looping")
//    }
//}
//arrLoop.append(1)
//testingLoopcount()

func maxSubArray(_ arr:[Int]) -> Int {
    if arr.count > 0 {
        

    var oldValue = 0
    var newValue = 0
        var negativeOld:Int?
    var negativecheck = true

    for obj in arr {
        if negativecheck {
            if obj < 0 {
                if let val = negativeOld {
                if obj > val {
                    negativeOld = obj
                }
            }
            else {
                negativeOld = obj
            }
            }
            else {
                negativecheck = false
                newValue = obj
                oldValue = newValue

            }
        }
        else {

            newValue += obj
       
        if newValue > oldValue {
            oldValue = newValue
           
        }
        
        if newValue < 0 {
            newValue = 0
        }
        }
    }
        
        if negativecheck {
            return negativeOld ?? 0
        }
        return oldValue

    }
    return 0
}

var testArr = [-2,1,-3,4,-1,2,1,-5,4]
var maxSubarry = maxSubArray(testArr)

var weight = [1,1,1]
var value = [10,20,30]
var maxVal = 0;
func KnapsackProblem() -> Int {

    return pickElement(0, index: -1, 0)
}
func pickElement(_ currentWt:Int,index:Int,_ currentVal:Int) -> Int {
    print(currentVal,currentWt)
    var max = 0;
    for i in index+1..<weight.count {
        var loopValue = 0
        var currentWt = currentWt
        var currentVal = currentVal

        if currentWt + weight[i] <= 2  {
            currentWt = currentWt + weight[i]
            currentVal = currentVal + value[i]
            loopValue = getMax(currentVal, pickElement(currentWt, index: i,currentVal))

        }
        max = getMax(max, loopValue)
    }
    return max;
}
func getMax(_ a:Int, _ b:Int) -> Int {
    if a > b {
        return a
    }
    return b
}

var dictClimbing:Dictionary<Int,Int> = [:]
var dictParrlel:Dictionary<Int,Int> = [:]


func climbingStairs(_ n:Int) -> Int {
    if n == 0 {
        return 0
    }
    if n == 1 {
        return 1
    }
    else {
         return moveBottom(n)
    }
}
func moveBottom(_ n:Int) -> Int {
    var n = n
    var count = 1
    while n > 1 {
        n -= 2

        if let val = dictClimbing[n] {
            count += val
            if let parrlel = dictParrlel[n] {
                print("val")
                count += parrlel
            }
            else {
            count += moveParrlel(n)
            }
            return count

        }
        else {
            count += 1

        if n > 0 {
            let parrelel = moveParrlel(n)
            dictParrlel[n] = parrelel
            count += parrelel
        }
        }
    }
    return count
}
func moveParrlel(_ n:Int) -> Int {
    var n = n
    var count = 0
    while n > 0 {
        n -= 1
        if n > 1 {
            if let parrlel = dictParrlel[n] {
                print("val")
                count += parrlel
                return count
            }
            if let val = dictClimbing[n] {
               count += val
            }
            else {
                dictClimbing[n] = moveBottom(n)
                count += dictClimbing[n]!
            }
        }
        else {
            count += 1
        }
  
    }
    return count
}
//var climbingStairsWays = climbingStairs(10)
func isAnagram(_ s: String, _ t: String) -> Bool {
    var dict:[Character:Int] = [:]
    var dictSecond:[Character:Int] = [:]

    for c in s.characters {
        if let val = dict[c] {
            dict[c] = val + 1
            continue
        }
        dict[c] = 1
    }
    for c in t.characters {
        if let val = dictSecond[c] {
            dictSecond[c] = val + 1
            continue
        }
        dictSecond[c] = 1
    }
    if dict == dictSecond {
        return true
    }
    return false
}
//isAnagram("aya", "yaa")

func mergeList(_ list1:ListNode?,_ list2:ListNode?) -> ListNode? {
    var p:ListNode?
    var a:ListNode?
    var s:ListNode?
    var head:ListNode?
    if list1 == nil && list2 == nil {
    }
    else if list1 == nil && list2 != nil {
        head = list2
    }
    else if  list1 != nil && list2 == nil {
        head = list1
    }
    else {
    
     s = list1
     a = list2
    if (list1?.val)! < (list2?.val)! {
        p = s
        if let val = s?.next {
            s = val
        }
        else {
        s = nil
        }
    }
    else {
        p = a
        if let val = a?.next {
            a = val
        }
        else {
            a = nil
        }
        
    }
    head = p

    while s != nil || a != nil {
        if s != nil && a != nil {
            
        if (s?.val)! < (a?.val)! {
            p?.next = s

            if let val = s?.next {
                s = val
            }
            else {
                s = nil
            }
            p = p?.next
           
        }
        else {
            p?.next = a
            if let val = a?.next {
                a = val
                
            }
            else {
                a = nil
            }
            p = p?.next

        }
            }
        else {
            if let sVal = s {
                p?.next = sVal
                if let val = s?.next {
                    s = val
                }
                else {
                    s = nil
                }
                return head
            }
            else {
            p?.next = a
            if let val = a?.next {
                a = val
                
            }
            else {
                a = nil
            }
                return head
            }
        }
    }
    }
    return head
}




func deleteDuplicates(_ head: ListNode?) -> ListNode? {
    if head?.next != nil {
        var p = head
        var a = head?.next
        var currentVal = head?.val
        while a != nil {
            if currentVal == a?.val {
                p?.next = a?.next
                a = a?.next
            }
            else {
                a = a?.next
                p = p?.next
                currentVal = p?.val

            }
        }
    }
    return head
    
}

var list12:ListNode? = ListNode(1)
list12?.next = ListNode(1)
list12?.next?.next = ListNode(1)
list12?.next?.next?.next = nil

list12?.next?.next?.next?.next = nil



func palindromList(_ head:ListNode?) -> Bool {
    if head?.next != nil {
    var p = head
    var dict:Dictionary<Int,Int> = [:]
    var position = 0
    while p != nil {
        position += 1
        dict[position] = p?.val
        p = p?.next
    }
    var halfCount = 0
    if position%2 == 0 {
        halfCount = position/2
    }
    else {
        halfCount = position/2 + 1
    }
    
    
    for i in 1...halfCount {
        let complement = (position - i) + 1
        if dict[i] != dict[complement] {
            return false
        }
    }
    }
    return true
}


func ReverseLinkList(_ head:ListNode?) -> ListNode? {
    if let val = head?.next {

        var a = head
        var p = val
        var f = p
        if p.next != nil {
        while let val = f.next {
            p.next = a
            a = p
            p = val
            f = p
        }
        f.next = a
        }
        else {
            p.next = a
        }
        head?.next = nil
        return p

        
    }
    return head
}
var list21 = ListNode(1)
list21.next = ListNode(2)
list21.next?.next = ListNode(2)
list21.next?.next?.next = ListNode(7)

//var lists = ReverseLinkList(list21)
func removeElements(_ head: ListNode?, _ val: Int) -> ListNode? {
    if head != nil {
        var p = head
        var a = p?.next
        while a != nil {
            if a?.val == val {
                p?.next = a?.next
                a = p?.next
            }
            else {
                a = a?.next
                p = p?.next
            }
        }
        if head?.val != val {
          return head
        }
        else {
           return head?.next
        }
    }
    return nil
}


